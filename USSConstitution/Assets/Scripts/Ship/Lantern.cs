﻿using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

using Framework;

namespace Constitution
{
    public class Lantern : MonoBehaviour
    {
        static readonly int k_emissionColorProperty = Shader.PropertyToID("_EmissiveColor");
        
        [SerializeField]
        Material m_diffusorMat;
        
        [SerializeField]
        [Range(0f, 100f)]
        float m_shadowDistance = 15f;

        [SerializeField]
        [MinMaxSlider(0f, 10f)]
        MinMaxRange m_flickerSpeedRange = new MinMaxRange(1f, 2f);

        [SerializeField]
        [Range(0f, 1f)]
        float m_flickerStrength = 0.5f;

        Material m_diffusorMatInstance;
        Light m_light;
        HDAdditionalLightData m_hdLight;
        Color m_emissionColor;
        float m_intensityScale;
        float m_flickerSpeed;
        Vector2 m_noiseOffset;

        void Awake()
        {
            m_diffusorMatInstance = new Material(m_diffusorMat);
            m_emissionColor = m_diffusorMatInstance.GetColor(k_emissionColorProperty);
            
            foreach (var renderer in GetComponentsInChildren<MeshRenderer>())
            {
                var materials = renderer.sharedMaterials;

                for (var i = 0; i < materials.Length; i++)
                {
                    if (materials[i] == m_diffusorMat)
                    {
                        materials[i] = m_diffusorMatInstance;
                    }
                }

                renderer.sharedMaterials = materials;
            }
            
            m_light = GetComponentInChildren<Light>();
            m_hdLight = m_light.GetComponent<HDAdditionalLightData>();
            m_hdLight.shadowFadeDistance = m_shadowDistance;
            
            m_intensityScale = m_hdLight.intensity;
            m_flickerSpeed = Random.Range(m_flickerSpeedRange.Min, m_flickerSpeedRange.Max);
            m_noiseOffset = Random.insideUnitSphere;
        }

        void LateUpdate()
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                m_light.enabled = !m_light.enabled;
            }
            if (Input.GetKeyDown(KeyCode.O))
            {
                m_light.shadows = m_light.shadows == LightShadows.None ? LightShadows.Soft : LightShadows.None;
            }

            var t = m_flickerSpeed * Time.time;
            var noise = Mathf.PerlinNoise(m_noiseOffset.x + t, m_noiseOffset.y + t);
            var intensity = Mathf.Lerp(1f, noise, m_flickerStrength);
            
            m_hdLight.intensity = m_intensityScale * intensity;
            m_diffusorMatInstance.SetColor(k_emissionColorProperty, m_emissionColor * intensity);
        }
    }
}
