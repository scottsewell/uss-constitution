﻿using System.Collections.Generic;

using UnityEngine;

namespace Constitution
{
    public class Ship : MonoBehaviour
    {
        struct GunState
        {
            public Gun gun;
            public Vector3 position;
        };

        [Header("Firing")]

        [SerializeField]
        bool m_autoFire = false;

        [SerializeField]
        [Range(0.1f, 50.0f)]
        float m_rollingFireSpeed = 5f;

        [SerializeField]
        [Range(0f, 7f)]
        float m_speed = 3.0f;

        [SerializeField]
        [Range(0f, 2f)]
        float m_waveScale = 0.5f;

        [SerializeField]
        Vector3 m_waveScaleRotation = new Vector3(1f, 1f, 1f);

        [SerializeField]
        [Range(0f, 1f)]
        float m_waveSpeed = 0.25f;

        readonly List<GunState> m_rightGuns = new List<GunState>();
        readonly List<GunState> m_leftGuns = new List<GunState>();

        WaterSurface m_ocean;
        
        void Awake()
        {
            foreach (var gun in GetComponentsInChildren<Gun>(true))
            {
                var state = new GunState
                {
                    gun = gun,
                    position = transform.InverseTransformPoint(gun.transform.position),
                };

                var side = (state.position.x < 0f) ?  m_leftGuns : m_rightGuns;
                side.Add(state);
            }
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.J))
            {
                m_autoFire = !m_autoFire;
            }

            if (m_autoFire)
            {
                FireCannons();
            }

            var pos = transform.position;
            pos += m_speed * Time.deltaTime * transform.forward;
            pos.y = m_waveScale * (-0.5f + Mathf.PerlinNoise(1.0f, Time.time * m_waveSpeed));
            transform.position = pos;

            var rot = Vector3.Scale(m_waveScale * m_waveScaleRotation, (-0.5f * Vector3.one) + new Vector3(
                Mathf.PerlinNoise(0.5f, Time.time * m_waveSpeed),
                Mathf.PerlinNoise(Time.time * m_waveSpeed, 1.0f),
                Mathf.PerlinNoise(2.0f, Time.time * m_waveSpeed)
            ));
            transform.rotation = Quaternion.Euler(rot);
        }

        void FireCannons()
        {
            FireCannons(m_rightGuns);
        }

        void FireCannons(List<GunState> guns)
        {
            // get the position of the first fireable cannon
            var rollStartPos = float.NegativeInfinity;

            foreach (var state in guns)
            {
                if (state.gun.CanFire && rollStartPos < state.position.z)
                {
                    rollStartPos = state.position.z;
                }
            }

            // fire all fireable cannons, delayed by distance relative to the first fireable cannon
            foreach (var state in guns)
            {
                if (state.gun.CanFire)
                {
                    state.gun.Fire((rollStartPos - state.position.z) / m_rollingFireSpeed);
                }
            }
        }
    }
}
