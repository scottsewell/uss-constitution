﻿using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Rendering;

namespace Constitution
{
    public class ReverbVolume : MonoBehaviour
    {
        [SerializeField]
        private AudioMixer m_mixer = null;

        [SerializeField]
        private AudioMixerSnapshot m_from = null;
        [SerializeField]
        private AudioMixerSnapshot m_to = null;

        private Volume m_volume = null;
        private AudioMixerSnapshot[] m_snapshots = new AudioMixerSnapshot[2];
        private float[] m_weights = new float[2];

        private void Awake()
        {
            m_volume = GetComponent<Volume>();
        }

        private void Update()
        {
            m_snapshots[0] = m_from;
            m_snapshots[1] = m_to;

            m_weights[0] = 1f - m_volume.weight;
            m_weights[1] = m_volume.weight;

            m_mixer.TransitionToSnapshots(m_snapshots, m_weights, 0f);
        }
    }
}
