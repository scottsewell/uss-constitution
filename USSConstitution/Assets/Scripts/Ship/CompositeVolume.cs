﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Constitution
{
    public class CompositeVolume : MonoBehaviour
    {
        [SerializeField]
        [Tooltip("The distance in meters over which the volume is blended in.")]
        [Range(0f, 5f)]
        float m_blendDistance = 1f;

        Collider[] m_volumes = null;
        Volume m_volume = null;

        void Awake()
        {
            m_volumes = GetComponentsInChildren<Collider>(true);
            m_volume = GetComponent<Volume>();
        }

        void OnEnable()
        {
            m_volume.enabled = true;
        }

        void OnDisable()
        {
            m_volume.enabled = false;
        }

        void Update()
        {
            var viewerPosition = Camera.main.transform.position;
            var minDistance = float.MaxValue;

            foreach (var volume in m_volumes)
            {
                var distance = Vector3.Distance(viewerPosition, volume.ClosestPoint(viewerPosition));

                if (minDistance > distance)
                {
                    minDistance = distance;
                }
            }

            m_volume.weight = Mathf.Clamp01(1f - (minDistance / m_blendDistance));
        }
    }
}
