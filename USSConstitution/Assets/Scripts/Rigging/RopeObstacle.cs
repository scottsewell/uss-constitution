﻿using UnityEngine;

namespace Constitution.Rigging
{
    public sealed class RopeObstacle : RopeFeature
    {
        [SerializeField]
        internal Vector3 m_edgeStart = Vector3.zero;
        [SerializeField]
        internal Vector3 m_edgeEnd = Vector3.up;
        [SerializeField, Range(-180f, 180f)]
        internal float m_rotation = 0f;
        [SerializeField]
        internal Transform m_outputTransform;
        
        public Vector3 EdgeStart => m_edgeStart * transform.localScale.x;
        
        public Vector3 EdgeEnd => m_edgeEnd * transform.localScale.x;

        public Vector3 EdgeNormal => Quaternion.AngleAxis(m_rotation, m_edgeEnd - m_edgeStart) * Vector3.forward;

        public Transform OutputTransform => m_outputTransform;
        
#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.matrix = transform.localToWorldMatrix;

            var c0 = EdgeStart;
            var c1 = EdgeStart + (EdgeNormal * 0.25f); 
            var c2 = EdgeEnd + (EdgeNormal * 0.25f);
            var c3 = EdgeEnd;
            
            Gizmos.DrawLine(c0, c1);
            Gizmos.DrawLine(c1, c2);
            Gizmos.DrawLine(c2, c3);
            Gizmos.DrawLine(c3, c0);
        }
#endif
    }
}
