﻿using UnityEditor;

using UnityEngine;

namespace Constitution.Rigging.Editor
{
    [CustomEditor(typeof(RopePoint))]
    class RopePointEditor : UnityEditor.Editor
    {
        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            var ropePoint = target as RopePoint;
            var transform = ropePoint.transform;

            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Transform to Origin"))
                {
                    Undo.RecordObjects(new Object[] { transform, ropePoint }, "Transform to Origin");
                    
                    var globalOffset = transform.TransformPoint(ropePoint.m_offset);

                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                    transform.localScale = Vector3.one;
                    
                    ropePoint.m_offset = transform.InverseTransformPoint(globalOffset);
                    ropePoint.OnParametersChanged();
                    
                    EditorUtility.SetDirty(transform);
                }
                if (GUILayout.Button("Transform to Point"))
                {
                    Undo.RecordObjects(new Object[] { transform, ropePoint }, "Transform to Point");
                    
                    var globalOffset = transform.TransformPoint(ropePoint.m_offset);

                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                    transform.localScale = Vector3.one;
                    
                    transform.localPosition = transform.InverseTransformPoint(globalOffset);
                    ropePoint.m_offset = Vector3.zero;
                    ropePoint.OnParametersChanged();

                    EditorUtility.SetDirty(transform);
                }
            }
        }

        void OnSceneGUI()
        {
            var ropePoint = target as RopePoint;
            var transform = ropePoint.transform;
            var offset = transform.TransformPoint(ropePoint.m_offset);
            var handleRot = Tools.pivotRotation == PivotRotation.Local ? transform.rotation : Quaternion.identity;

            EditorGUI.BeginChangeCheck();
            
            var newOffset = Handles.DoPositionHandle(offset, handleRot);
            
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(ropePoint, "Set Rope Point Position");
                ropePoint.m_offset = transform.InverseTransformPoint(newOffset);
                ropePoint.OnParametersChanged();
                EditorUtility.SetDirty(transform);
            }
        }
    }
}
