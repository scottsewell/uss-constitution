﻿using System;

using UnityEditor;

using UnityEditorInternal;

using UnityEngine;

namespace Constitution.Rigging.Editor
{
    [CustomEditor(typeof(Rope))]
    class RopeEditor : UnityEditor.Editor
    {
        const float k_inchesToMeters = 0.0254f;
        
        SerializedProperty m_radius;
        SerializedProperty m_resolution;
        SerializedProperty m_features;

        ReorderableList m_featuresList;

        void OnEnable()
        {
            m_radius = serializedObject.FindProperty(nameof(m_radius));
            m_resolution = serializedObject.FindProperty(nameof(m_resolution));
            m_features = serializedObject.FindProperty(nameof(m_features));

            m_featuresList = new ReorderableList(serializedObject, m_features)
            {
                drawHeaderCallback = (rect) =>
                {
                    EditorGUI.LabelField(rect, "Features");
                },
                elementHeightCallback = (index) =>
                {
                    var element = m_features.GetArrayElementAtIndex(index);
                    var feature = element.objectReferenceValue as RopeFeature;

                    switch (feature)
                    {
                        case Sheeve:
                        {
                            return 2 * EditorGUIUtility.singleLineHeight;
                        }
                        case RopeObstacle:
                        {
                            return 2 * EditorGUIUtility.singleLineHeight;
                        }
                        default:
                        {
                            return EditorGUIUtility.singleLineHeight;
                        }
                    }
                },
                elementHeight = EditorGUIUtility.singleLineHeight,
                drawElementCallback = (rect, index, active, focused) =>
                {
                    var element = m_features.GetArrayElementAtIndex(index);
                    var feature = element.objectReferenceValue as RopeFeature;

                    rect.height = EditorGUIUtility.singleLineHeight;
                    
                    var objectRect = rect;
                    var featureRect = rect;
                    objectRect.width /= 2;
                    featureRect.xMin = objectRect.xMax;

                    using (var change = new EditorGUI.ChangeCheckScope())
                    {
                        var newFeature = EditorGUI.ObjectField(objectRect, feature, typeof(RopeFeature), true) as RopeFeature;
                        
                        if (change.changed)
                        {
                            element.objectReferenceValue = newFeature;
                        }
                    }

                    if (feature == null)
                    {
                        return;
                    }
                    
                    var components = feature.gameObject.GetComponents<RopeFeature>();

                    if (components.Length > 1)
                    {
                        var selectedIndex = Array.IndexOf(components, feature);

                        using (var change = new EditorGUI.ChangeCheckScope())
                        {
                            selectedIndex = EditorGUI.IntSlider(featureRect, selectedIndex + 1, 1, components.Length) - 1;
                        
                            if (change.changed)
                            {
                                element.objectReferenceValue = components[selectedIndex];
                            }
                        }
                    }

                    var labelRect = rect;
                    var valueRect = rect;
                    labelRect.width /= 2;
                    valueRect.xMin = labelRect.xMax;

                    labelRect.y += EditorGUIUtility.singleLineHeight;
                    valueRect.y += EditorGUIUtility.singleLineHeight;
                    
                    switch (feature)
                    {
                        case Sheeve sheeve:
                        {
                            EditorGUI.LabelField(labelRect, EditorGUIUtility.TrTempContent("Invert Direction"));
                            
                            using (var change = new EditorGUI.ChangeCheckScope())
                            {
                                var value = EditorGUI.Toggle(valueRect, sheeve.InvertDirection);
                        
                                if (change.changed)
                                {
                                    Undo.RecordObject(sheeve, "Edit Rope Feature");
                                    sheeve.InvertDirection = value;
                                    EditorUtility.SetDirty(sheeve);
                                }
                            }
                            break;
                        }
                        case RopeObstacle obstacle:
                        {
                            EditorGUI.LabelField(labelRect, EditorGUIUtility.TrTempContent("Output Transform"));
                            
                            using (var change = new EditorGUI.ChangeCheckScope())
                            {
                                var value = EditorGUI.ObjectField(valueRect, obstacle.m_outputTransform, typeof(Transform), true) as Transform;
                        
                                if (change.changed)
                                {
                                    Undo.RecordObject(obstacle, "Edit Rope Feature");
                                    obstacle.m_outputTransform = value;
                                    obstacle.OnParametersChanged();
                                    EditorUtility.SetDirty(obstacle);
                                }
                            }
                            break;
                        }
                    }
                },
            };
        }

        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            EditorGUILayout.PropertyField(m_radius);
            
            var radiusInches = m_radius.floatValue / k_inchesToMeters;
            var circumferenceInches = 2f * Mathf.PI * radiusInches;

            var rect = EditorGUILayout.GetControlRect();
            rect = EditorGUI.PrefixLabel(rect, EditorGUIUtility.TrTempContent("Circumference"));
            EditorGUI.LabelField(rect, new GUIContent($"{circumferenceInches:n3}\""));
            
            EditorGUILayout.PropertyField(m_resolution);
            
            m_featuresList.DoLayoutList();
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}
