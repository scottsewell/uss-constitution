﻿using UnityEditor;

namespace Constitution.Rigging.Editor
{
    [CustomEditor(typeof(RopeGroup))]
    class RopeGroupEditor : UnityEditor.Editor
    {
        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var group = target as RopeGroup;

            EditorGUILayout.LabelField("Ropes", EditorStyles.boldLabel);
            
            for (var i = 0; i < group.m_ropes.Count; i++)
            {
                var rope = group.m_ropes[i];     

                if (rope != null)
                {
                    EditorGUILayout.LabelField(rope.name);
                }
            }
        }
    }
}
