﻿using UnityEditor;

using UnityEngine;

namespace Constitution.Rigging.Editor
{
    [CustomEditor(typeof(RopeObstacle))]
    class RopeObstacleEditor : UnityEditor.Editor
    {
        /// <inheritdoc />
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            var obstacle = target as RopeObstacle;
            var transform = obstacle.transform;

            using (new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Transform to Origin"))
                {
                    Undo.RecordObjects(new Object[] { transform, obstacle }, "Transform to Origin");
                    
                    var globalEdgeStart = transform.TransformPoint(obstacle.m_edgeStart);
                    var globalEdgeEnd = transform.TransformPoint(obstacle.m_edgeEnd);

                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                    transform.localScale = Vector3.one;
                    
                    obstacle.m_edgeStart = transform.InverseTransformPoint(globalEdgeStart);
                    obstacle.m_edgeEnd = transform.InverseTransformPoint(globalEdgeEnd);
                    obstacle.OnParametersChanged();
                    
                    EditorUtility.SetDirty(transform);
                }
                if (GUILayout.Button("Transform to Point"))
                {
                    Undo.RecordObjects(new Object[] { transform, obstacle }, "Transform to Point");
                    
                    var globalEdgeStart = transform.TransformPoint(obstacle.m_edgeStart);
                    var globalEdgeEnd = transform.TransformPoint(obstacle.m_edgeEnd);

                    transform.localPosition = Vector3.zero;
                    transform.localRotation = Quaternion.identity;
                    transform.localScale = Vector3.one;
                    
                    transform.localPosition = transform.InverseTransformPoint(0.5f * (globalEdgeStart + globalEdgeEnd));
                    obstacle.m_edgeStart = transform.InverseTransformPoint(globalEdgeStart);
                    obstacle.m_edgeEnd = transform.InverseTransformPoint(globalEdgeEnd);
                    obstacle.OnParametersChanged();

                    EditorUtility.SetDirty(transform);
                }
                if (GUILayout.Button("Mirror"))
                {
                    Undo.RecordObjects(new Object[] { transform, obstacle }, "Mirror");

                    var position = transform.localPosition;
                    position.x *= -1f;
                    transform.localPosition = position;
                    
                    var rotation = transform.localEulerAngles;
                    rotation.y *= -1f;
                    rotation.z *= -1f;
                    transform.localEulerAngles = rotation;
                    
                    obstacle.m_edgeStart.x *= -1f;
                    obstacle.m_edgeEnd.x *= -1f;
                    obstacle.OnParametersChanged();

                    EditorUtility.SetDirty(transform);
                }
            }
        }

        void OnSceneGUI()
        {
            var obstacle = target as RopeObstacle;
            var transform = obstacle.transform;
            var handleRot = Tools.pivotRotation == PivotRotation.Local ? transform.rotation : Quaternion.identity;

            EditorGUI.BeginChangeCheck();
            
            var start = Handles.DoPositionHandle(transform.TransformPoint(obstacle.m_edgeStart), handleRot);
            
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(obstacle, "Set Rope Point Position");
                obstacle.m_edgeStart = transform.InverseTransformPoint(start);
                obstacle.OnParametersChanged();
                EditorUtility.SetDirty(transform);
            }
            
            var end = Handles.DoPositionHandle(transform.TransformPoint(obstacle.m_edgeEnd), handleRot);
            
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(obstacle, "Set Rope Point Position");
                obstacle.m_edgeEnd = transform.InverseTransformPoint(end);
                obstacle.OnParametersChanged();
                EditorUtility.SetDirty(transform);
            }
        }
    }
}
