﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

namespace Constitution.Rigging
{
    public sealed class Sheeve : RopeFeature
    {
        [SerializeField]
        Vector3 m_offset;
        [SerializeField, Range(0.01f, 0.5f)]
        float m_radius = 0.1f;
        [SerializeField]
        bool m_invertDirection;
        [SerializeField, Range(10f, 90f)]
        float m_degreesPerVertex = 45f;

        public Vector3 LocalOffset => m_offset * transform.localScale.x;
        
        public float Radius => m_radius * transform.localScale.x;
        
        public bool InvertDirection
        {
            get => m_invertDirection;
            set
            {
                if (m_invertDirection != value)
                {
                    m_invertDirection = value;
                    OnParametersChanged();
                }
            }
        }
        
        public float DegreesPerVertex => m_degreesPerVertex;
        
#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            Handles.color = Color.green;
            Handles.DrawWireDisc(transform.TransformPoint(m_offset), transform.right, Radius);
        }
        
        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;

            for (var i = 0; i < 8; i++)
            {
                var rotation = Quaternion.Euler(45 * i, 0, 0);
                var start = m_offset + (rotation * new Vector3(0f, m_radius, 0f));
                var end = m_offset + (rotation * new Vector3(0f, m_radius, m_radius * (m_invertDirection ? -0.75f : 0.75f)));
            
                Gizmos.DrawLine(transform.TransformPoint(start), transform.TransformPoint(end));
            }
        }
#endif
    }
}
