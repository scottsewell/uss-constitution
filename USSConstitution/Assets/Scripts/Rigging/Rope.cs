﻿using System.Linq;

using UnityEngine;

namespace Constitution.Rigging
{
    [ExecuteAlways]
    public class Rope : MonoBehaviour
    {
        [SerializeField, Range(0.005f, 0.1f)]
        float m_radius = 0.025f;
        [SerializeField, Range(3, 12)]
        int m_resolution = 8;
        [SerializeField]
        RopeFeature[] m_features;

        RopeGroup m_group;
        bool m_dirty;
        
        public float Radius => m_radius;
        public int Resolution => m_resolution;

        void OnEnable()
        {
            m_group = GetComponentInParent<RopeGroup>();

            if (m_group != null && IsValid())
            {
                m_group.RegisterRope(this);
            }

            if (m_features != null)
            {
                foreach (var feature in m_features)
                {
                    if (feature != null)
                    {
                        feature.ParametersChanged += MarkDirty;
                    }
                }
            }
        }

        void OnDisable()
        {
            if (m_group != null)
            {
                m_group.DeregisterRope(this);
            }
            
            if (m_features != null)
            {
                foreach (var feature in m_features)
                {
                    if (feature != null)
                    {
                        feature.ParametersChanged -= MarkDirty;
                    }
                }
            }
        }

#if UNITY_EDITOR
        void OnValidate()
        {
            m_dirty = true;
        }

        void Update()
        {
            if (m_group != null && m_dirty)
            {
                m_group.DeregisterRope(this);

                if (IsValid())
                {
                    m_group.RegisterRope(this);
                }

                m_dirty = false;
            }
        }
#endif

        internal int GetFeatureCount()
        {
            return m_features.Length;
        }

        internal RopeFeature GetFeature(int index)
        {
            return m_features[index];
        }

        internal void MarkDirty()
        {
            m_dirty = true;
        }

        bool IsValid()
        {
            if (m_features == null || m_features.Length <= 1)
            {
                return false;
            }
            
            for (var i = 0; i < m_features.Length; i++)
            {
                if (m_features[i] == null)
                {
                    return false;
                }
            }

            if (!(m_features.First() is RopePoint && m_features.Last() is RopePoint))
            {
                return false;
            }

            return true;
        }
    }
}
