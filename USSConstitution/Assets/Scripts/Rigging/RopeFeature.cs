﻿using System;

using UnityEngine;

namespace Constitution.Rigging
{
    public abstract class RopeFeature : MonoBehaviour
    {
        public event Action ParametersChanged;

        protected virtual void OnValidate()
        {
            OnParametersChanged();
        }
        
        internal void OnParametersChanged()
        {
            ParametersChanged?.Invoke();
        }
    }
}
