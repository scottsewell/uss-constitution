﻿using UnityEngine;

namespace Constitution.Rigging
{
    public sealed class RopeSlack : RopeFeature
    {
        [SerializeField, Range(1, 32)]
        internal int m_resolution = 12;
        [SerializeField, Range(0f, 10f)]
        internal float m_slack = 1f;
        
        public int Resolution => m_resolution;
        
        public float Slack => m_slack;
    }
}
