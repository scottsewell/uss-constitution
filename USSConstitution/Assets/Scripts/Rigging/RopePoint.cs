﻿using UnityEngine;

namespace Constitution.Rigging
{
    public sealed class RopePoint : RopeFeature
    {
        [SerializeField]
        internal Vector3 m_offset;
        
        public Vector3 LocalOffset => m_offset * transform.localScale.x;
        
#if UNITY_EDITOR
        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.TransformPoint(m_offset), 0.05f);
        }
#endif
    }
}
