﻿using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;            
#endif

using UnityEngine;
using UnityEngine.Rendering;

namespace Constitution.Rigging
{
    static class RopeManager
    {
        static RopeManager()
        {
            Application.quitting += Cleanup;
#if UNITY_EDITOR
            AssemblyReloadEvents.beforeAssemblyReload += Cleanup;
#endif
        }
        
        static bool s_initialized;
        static bool s_enabled;
        
        internal static void Init()
        {
            // In case the domain reload on enter play mode is disabled, we must
            // reset all static fields.
            Cleanup();

            // enable the rope renderer by default
            Enable();
        }
        
        static void Cleanup()
        {
            Disable();
            DisposeRegistrar();
        }

        /// <summary>
        /// Enable the rope renderer.
        /// </summary>
        public static void Enable()
        {
            if (s_enabled)
            {
                return;
            }
            
            if (!CreateResources())
            {
                return;
            }
            
            s_enabled = true;
            
            RenderPipelineManager.beginFrameRendering += OnBeginFrameRendering;
            RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;
        }

        /// <summary>
        /// Disable the rope renderer.
        /// </summary>
        public static void Disable()
        {
            if (!s_enabled)
            {
                return;
            }
            
            s_enabled = false;
            
            RenderPipelineManager.beginFrameRendering -= OnBeginFrameRendering;
            RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;

            DisposeResources();
        }

        /// <summary>
        /// Registers a rope to the manager.
        /// </summary>
        /// <param name="rope">The rope to register.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="rope"/> is null.</exception>
        // public static void RegisterRope(IRope rope)
        // {
        //     if (rope == null)
        //     {
        //         throw new ArgumentNullException(nameof(rope));
        //     }
        //     
        //     for (var i = 0; i < s_ropeGroups.Count; i++)
        //     {
        //         if (s_ropeGroups[i].AddRope(rope))
        //         {
        //             return;
        //         }
        //     }
        //
        //     var group = new RopeGroupState(rope.Material);
        //     group.AddRope(rope);
        //     s_ropeGroups.Add(group);
        // }

        /// <summary>
        /// Deregisters a rope from the manager.
        /// </summary>
        /// <param name="rope">The rope to deregister.</param>
        /// <exception cref="ArgumentNullException">Thrown if <paramref name="rope"/> is null.</exception>
        // public static void DeregisterRope(IRope rope)
        // {
        //     if (rope == null)
        //     {
        //         throw new ArgumentNullException(nameof(rope));
        //     }
        //
        //     for (var i = 0; i < s_ropeGroups.Count; i++)
        //     {
        //         if (s_ropeGroups[i].RemoveRope(rope))
        //         {
        //             return;
        //         }
        //     }
        // }

        /// <summary>
        /// Prepares the ropes for rendering.
        /// </summary>
        internal static void Update()
        {
            if (!s_enabled)
            {
                return;
            }

            // check which buffers need to be updated, if any
            // for (var i = 0; i < s_ropeGroups.Count; i++)
            // {
            //     var ropeGroup = s_ropeGroups[i];
            //
            //     ropeGroup.Update();
            // }
        }
        
        static void OnBeginFrameRendering(ScriptableRenderContext context, Camera[] cameras)
        {
            // if (s_instanceCount == 0)
            // {
            //     return;
            // }
            //
            // PrepareFrameRendering();
        }

        static void OnBeginCameraRendering(ScriptableRenderContext context, Camera camera)
        {
            // if (s_instanceCount == 0)
            // {
            //     return;
            // }
            //
            // Render(camera);
        }

        static bool CreateResources()
        {
            if (s_initialized)
            {
                return true;
            }

            s_initialized = true;
            return true;
        }

        static void DisposeResources()
        {
            s_initialized = false;
        }
        
        static void DisposeRegistrar()
        {
            // for (var i = 0; i < s_ropeGroups.Count; i++)
            // {
            //     s_ropeGroups[i].Dispose();
            // }
            //
            // s_ropeGroups.Clear();
        }
    }
}
