﻿using System.Runtime.CompilerServices;

using Unity.Mathematics;

namespace Constitution.Rigging
{
    static class mathExt
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float asinh(float x)
        {
            return math.log(x + math.sqrt((x * x) + 1));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float catenary(float a, float x)
        {
            return a * math.cosh(x / a);
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static quaternion fromToRotation(float3 from, float3 to)
        {
            return quaternion.AxisAngle(
                math.normalize(math.cross(from, to)),
                math.acos(math.clamp(math.dot(math.normalize(from), math.normalize(to)), -1f, 1f))
            );
        }
    }
}
