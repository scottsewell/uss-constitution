﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

using Framework;

using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Jobs;
using UnityEngine.Rendering;

namespace Constitution.Rigging
{
    [ExecuteAlways]
    public class RopeGroup : MonoBehaviour
    {
        [Flags]
        enum DirtyFlags
        {
            None = 0,
            Resources           = 1 << 0,
            FeatureTransforms   = 1 << 1,
            VertexPositions     = 1 << 2,
            All = ~0,
        }

        enum FeatureType
        {
            Point,
            Slack,
            Obstacle,
            Sheeve,
        }

        struct FeatureTransform
        {
            public float3 position;
            public quaternion rotation;
        }

        struct PointData
        {
            public float3 localOffset;
        }

        struct SlackData
        {
            public int resolution;
            public float slack;
        }
        
        struct ObstacleData
        {
            public float3 edgeStart;
            public float3 edgeEnd;
            public float3 edgeNormal;
            public int outputIndex;
        }
        
        struct SheeveData
        {
            public float3 localOffset;
            public float radius;
            public bool invertDirection;
            public float radiansPerVertex;
        }

        [StructLayout(LayoutKind.Sequential, Pack = 1, Size = 32)]
        struct Vertex32
        {
            public static readonly VertexAttributeDescriptor[] k_layout =
            {
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 4),
                new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.SNorm8, 4),
                new VertexAttributeDescriptor(VertexAttribute.Tangent, VertexAttributeFormat.SNorm8, 4),
                new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, 2),
            };

            public float4 pos;
            
            public sbyte nrmX;
            public sbyte nrmY;
            public sbyte nrmZ;
            public sbyte nrmW;

            public sbyte tanX;
            public sbyte tanY;
            public sbyte tanZ;
            public sbyte tanW;

            public float2 uv;
        }
        
        [SerializeField]
        Material m_material;
        [SerializeField]
        Vector3 m_boundsCenter = Vector3.zero;
        [SerializeField]
        Vector3 m_boundsSize = Vector3.one;

        internal readonly List<Rope> m_ropes = new List<Rope>();
        DirtyFlags m_dirtyFlags;
        bool m_resourcesCreated;

        Mesh m_mesh;
        
        NativeArray<int> m_resolutions;
        NativeArray<float> m_radii;
        NativeArray<int> m_vertexIndices;
        NativeArray<int> m_vertexCounts;
        NativeArray<int> m_featureIndices;
        NativeArray<int> m_featureCounts;
        NativeArray<float3> m_twistHints;
        NativeArray<int> m_compactedVertexIndices;
        NativeArray<int> m_compactedIndexIndices;
        
        NativeArray<FeatureType> m_featureTypes;
        NativeArray<int> m_featureDataIndices;
        NativeArray<int> m_featureTransformIndices;
        
        NativeArray<PointData> m_pointData;
        
        NativeArray<SlackData> m_slackData;

        NativeArray<ObstacleData> m_obstacleData;
        NativeArray<float3> m_obstaclePositions;
        TransformAccessArray m_obstacleTransforms;
        
        NativeArray<SheeveData> m_sheeveData;
        
        NativeArray<FeatureTransform> m_featureTransforms;
        TransformAccessArray m_transforms;

        NativeArray<float3> m_vertices;
        
        NativeArray<int> m_vertexCount;
        NativeArray<int> m_indexCount;
        
        NativeArray<Vertex32> m_vertexBuffer;
        NativeArray<ushort> m_indexBuffer;

        UpdateFeatureDataJob m_updateFeatureDataJob;
        ComputeRopeVerticesJob m_computeRopeVerticesJob;
        WritePositionJob m_writeObstaclePositionsJob;
        CompactRopeIndicesJob m_compactRopeIndicesJob;
        ComputeRopeVertexBufferJob m_computeRopeVertexBufferJob;
        ComputeRopeIndexBufferJob m_computeRopeIndexBufferJob;

        void OnValidate()
        {
            m_dirtyFlags = DirtyFlags.All;
        }

        void OnEnable()
        {
            m_dirtyFlags = DirtyFlags.All;
        }

        void OnDisable()
        {
            DisposeResources();
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.DrawWireCube(m_boundsCenter, m_boundsSize);
        }

        void LateUpdate()
        {
            m_dirtyFlags |= DirtyFlags.FeatureTransforms | DirtyFlags.VertexPositions;
            
            if (UpdateGroup(out var updateJob))
            {
                updateJob.Complete();
                UpdateMesh();

                var filter = GetComponent<MeshFilter>();
                
                if (filter == null)
                {
                    filter = gameObject.AddComponent<MeshFilter>();
                    filter.hideFlags = HideFlags.DontSave;
                }
                
                filter.sharedMesh = m_mesh;
                
                var renderer = GetComponent<MeshRenderer>();

                if (renderer == null)
                {
                    renderer = gameObject.AddComponent<MeshRenderer>();
                    renderer.hideFlags = HideFlags.DontSave;
                }

                renderer.rayTracingMode = RayTracingMode.Off;
                renderer.sharedMaterial = m_material;
            }
        }

        internal bool UpdateGroup(out JobHandle updateJob)
        {
            updateJob = default;

            if (m_dirtyFlags == DirtyFlags.None)
            {
                return false;
            }

            if (!m_resourcesCreated || m_mesh == null || m_dirtyFlags.Contains(DirtyFlags.Resources))
            {
                CreateResources();
            }
            
            if (m_dirtyFlags.Contains(DirtyFlags.FeatureTransforms))
            {
                m_updateFeatureDataJob.worldToMeshLocalMatrix = transform.worldToLocalMatrix;
                m_updateFeatureDataJob.worldToMeshLocalRotation = math.inverse(transform.rotation);
                
                updateJob = m_updateFeatureDataJob.ScheduleReadOnlyByRef(m_transforms, 8);
            }

            if (m_dirtyFlags.Contains(DirtyFlags.VertexPositions))
            {
                var verticesJob = m_computeRopeVerticesJob.ScheduleByRef(m_ropes.Count, 1, updateJob);
                var writeObstaclesJob = m_writeObstaclePositionsJob.ScheduleByRef(m_obstacleTransforms, verticesJob);
                var compactJob = m_compactRopeIndicesJob.ScheduleByRef(verticesJob);
                var vertexBufferJob = m_computeRopeVertexBufferJob.ScheduleByRef(m_ropes.Count, 1, compactJob);
                var indexBufferJob = m_computeRopeIndexBufferJob.ScheduleByRef(m_ropes.Count, 1, compactJob);
                
                updateJob = JobHandle.CombineDependencies(writeObstaclesJob, vertexBufferJob, indexBufferJob);
            }

            m_dirtyFlags = DirtyFlags.None;
            return true;
        }

        internal void UpdateMesh()
        {
            var bounds = new Bounds(m_boundsCenter, m_boundsSize);
            
            var vertexCount = m_vertexCount[0];
            var indexCount = m_indexCount[0];
                
            m_mesh.SetVertexBufferParams(vertexCount, Vertex32.k_layout);
            m_mesh.SetIndexBufferParams(indexCount, IndexFormat.UInt16);
            
            m_mesh.SetVertexBufferData(
                m_vertexBuffer, 0, 0, vertexCount, 0,
                MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontValidateIndices
            );
            m_mesh.SetIndexBufferData(
                m_indexBuffer, 0, 0, indexCount,
                MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontValidateIndices
            );
            m_mesh.SetSubMesh(
                0,
                new SubMeshDescriptor
                {
                    bounds = bounds,
                    topology = MeshTopology.Triangles,
                    vertexCount = vertexCount,
                    indexCount = indexCount,
                },
                MeshUpdateFlags.DontRecalculateBounds | MeshUpdateFlags.DontValidateIndices
            );
            m_mesh.bounds = bounds;
            
            m_mesh.UploadMeshData(false);
        }
        
        public void RegisterRope(Rope rope)
        {
            if (rope != null && !m_ropes.Contains(rope))
            {
                m_ropes.Add(rope);
                m_dirtyFlags |= DirtyFlags.All;
            }
        }

        public void DeregisterRope(Rope rope)
        {
            if (rope != null && m_ropes.Remove(rope))
            {
                m_dirtyFlags |= DirtyFlags.All;
            }
        }

        void CreateResources()
        {
            if (m_resourcesCreated)
            {
                DisposeResources();
            }

            // compute the size of the data arrays
            var ropeCount = m_ropes.Count;
            var featureTransforms = new Dictionary<Transform, int>();
            var pointCount = 0;
            var slackCount = 0;
            var obstacleCount = 0;
            var sheeveCount = 0;
            var maxVertexCount = 0;
            var maxResolution = 0;
            
            for (var i = 0; i < ropeCount; i++)
            {
                var rope = m_ropes[i];
                var ropeFeatureCount = rope.GetFeatureCount();
                
                for (var j = 0; j < ropeFeatureCount; j++)
                {
                    var feature = rope.GetFeature(j);
                    var transform = feature.transform;

                    if (!featureTransforms.ContainsKey(transform))
                    {
                        featureTransforms.Add(transform, featureTransforms.Count);
                    }
                    
                    switch (feature)
                    {
                        case RopePoint point:
                        {
                            pointCount++;
                            maxVertexCount++;
                            break;
                        }
                        case RopeSlack slack:
                        {
                            slackCount++;
                            maxVertexCount += slack.Resolution;
                            break;
                        }
                        case RopeObstacle obstacle:
                        {
                            obstacleCount++;
                            maxVertexCount++;
                            break;
                        }
                        case Sheeve sheeve:
                        {
                            sheeveCount++;
                            maxVertexCount += Mathf.CeilToInt(360f / sheeve.DegreesPerVertex) + 1;
                            break;
                        }
                        default:
                        {
                            Debug.LogError($"Invalid rope: {rope}");
                            break;
                        }
                    }
                }
                
                maxResolution = Mathf.Max(maxResolution, rope.Resolution);
            }
            
            var featureCount = pointCount + slackCount + obstacleCount + sheeveCount;
            var maxVerticesPerRing = maxResolution + 1;
            var maxMeshVertexCount = maxVertexCount * maxVerticesPerRing;
            var maxMeshIndexCount = 6 * maxVerticesPerRing * Mathf.Max(maxVertexCount - ropeCount, 0);

            // allocate the data arrays
            m_resolutions = new NativeArray<int>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_radii = new NativeArray<float>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_vertexIndices = new NativeArray<int>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_vertexCounts = new NativeArray<int>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_featureIndices = new NativeArray<int>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_featureCounts = new NativeArray<int>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_twistHints = new NativeArray<float3>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_compactedVertexIndices = new NativeArray<int>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_compactedIndexIndices = new NativeArray<int>(
                ropeCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            
            m_featureTypes = new NativeArray<FeatureType>(
                featureCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_featureDataIndices = new NativeArray<int>(
                featureCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_featureTransformIndices = new NativeArray<int>(
                featureCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            
            m_pointData = new NativeArray<PointData>(
                pointCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            
            m_slackData = new NativeArray<SlackData>(
                slackCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            
            m_obstacleData = new NativeArray<ObstacleData>(
                obstacleCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_obstaclePositions = new NativeArray<float3>(
                obstacleCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_obstacleTransforms = new TransformAccessArray(obstacleCount);
            
            m_sheeveData = new NativeArray<SheeveData>(
                sheeveCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            
            m_featureTransforms = new NativeArray<FeatureTransform>(
                featureTransforms.Count,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_transforms = new TransformAccessArray(featureTransforms.Keys.ToArray());

            m_vertices = new NativeArray<float3>(
                maxVertexCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );

            m_vertexCount = new NativeArray<int>(
                1,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_indexCount = new NativeArray<int>(
                1,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );

            m_vertexBuffer = new NativeArray<Vertex32>(
                maxMeshVertexCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );
            m_indexBuffer = new NativeArray<ushort>(
                maxMeshIndexCount,
                Allocator.Persistent,
                NativeArrayOptions.UninitializedMemory
            );

            // populate data arrays that do not change
            var vertexIndex = 0;
            var featureIndex = 0;
            var pointIndex = 0;
            var slackIndex = 0;
            var obstacleIndex = 0;
            var obstacleOutputIndex = 0;
            var sheeveIndex = 0;

            for (var i = 0; i < ropeCount; i++)
            {
                var rope = m_ropes[i];
                var ropeFeatureCount = rope.GetFeatureCount();

                m_resolutions[i] = rope.Resolution;
                m_radii[i] = rope.Radius;
                m_vertexIndices[i] = vertexIndex;
                m_featureIndices[i] = featureIndex;
                m_featureCounts[i] = ropeFeatureCount;

                for (var j = 0; j < ropeFeatureCount; j++)
                {
                    var feature = rope.GetFeature(j);

                    m_featureTransformIndices[featureIndex] = featureTransforms[feature.transform];

                    switch (feature)
                    {
                        case RopePoint point:
                        {
                            m_featureTypes[featureIndex] = FeatureType.Point;
                            m_featureDataIndices[featureIndex] = pointIndex;
                            m_pointData[pointIndex++] = new PointData
                            {
                                localOffset = point.LocalOffset,
                            };
                            vertexIndex++;
                            break;
                        }
                        case RopeSlack slack:
                        {
                            m_featureTypes[featureIndex] = FeatureType.Slack;
                            m_featureDataIndices[featureIndex] = slackIndex;
                            m_slackData[slackIndex++] = new SlackData
                            {
                                resolution = slack.Resolution,
                                slack = slack.Slack,
                            };
                            vertexIndex += slack.Resolution;
                            break;
                        }
                        case RopeObstacle obstacle:
                        {
                            m_featureTypes[featureIndex] = FeatureType.Obstacle;
                            m_featureDataIndices[featureIndex] = obstacleIndex;
                            m_obstacleData[obstacleIndex++] = new ObstacleData
                            {
                                edgeStart = obstacle.EdgeStart,
                                edgeEnd = obstacle.EdgeEnd,
                                edgeNormal = obstacle.EdgeNormal,
                                outputIndex = obstacle.OutputTransform != null ? obstacleOutputIndex++ : -1,
                            };
                            if (obstacle.OutputTransform != null)
                            {
                                m_obstacleTransforms.Add(obstacle.OutputTransform);
                            }
                            vertexIndex++;
                            break;
                        }
                        case Sheeve sheeve:
                        {
                            m_featureTypes[featureIndex] = FeatureType.Sheeve;
                            m_featureDataIndices[featureIndex] = sheeveIndex;
                            m_sheeveData[sheeveIndex++] = new SheeveData
                            {
                                localOffset = sheeve.LocalOffset,
                                radius = sheeve.Radius,
                                invertDirection = sheeve.InvertDirection,
                                radiansPerVertex = Mathf.Deg2Rad * sheeve.DegreesPerVertex,
                            };
                            vertexIndex += Mathf.CeilToInt(360f / sheeve.DegreesPerVertex) + 1;
                            break;
                        }
                    }

                    featureIndex++;
                }
            }
            
            // create the mesh used for rendering
            m_mesh = new Mesh
            {
                name = name,
            };
            m_mesh.MarkDynamic();
            
            // create the jobs and populate fields that don't change
            m_updateFeatureDataJob = new UpdateFeatureDataJob
            {
                featureTransforms = m_featureTransforms,
            };
            m_computeRopeVerticesJob = new ComputeRopeVerticesJob
            {
                outputIndices = m_vertexIndices,
                featureIndices = m_featureIndices,
                featureCounts = m_featureCounts,
                featureTypes = m_featureTypes,
                featureDataIndices = m_featureDataIndices,
                featureTransformIndices = m_featureTransformIndices,
                pointData = m_pointData,
                slackData = m_slackData,
                obstacleData = m_obstacleData,
                sheeveData = m_sheeveData,
                featureTransforms = m_featureTransforms,
                obstaclePositions = m_obstaclePositions,
                twistHints = m_twistHints,
                vertexCounts = m_vertexCounts,
                vertices = m_vertices,
            };
            m_writeObstaclePositionsJob = new WritePositionJob
            {
                positions = m_obstaclePositions,
            };
            m_compactRopeIndicesJob = new CompactRopeIndicesJob
            {
                ropeCount = ropeCount,
                resolutions = m_resolutions,
                ringCounts = m_vertexCounts,
                vertexCount = m_vertexCount,
                indexCount = m_indexCount,
                vertexOffsets = m_compactedVertexIndices,
                indexOffsets = m_compactedIndexIndices,
            };
            m_computeRopeVertexBufferJob = new ComputeRopeVertexBufferJob
            {
                resolutions = m_resolutions,
                radii = m_radii,
                twistHints = m_twistHints,
                ringCounts = m_vertexCounts,
                ringPositions = m_vertices,
                ringOffsets = m_vertexIndices,
                vertexOffsets = m_compactedVertexIndices,
                vertexBuffer = m_vertexBuffer,
            };
            m_computeRopeIndexBufferJob = new ComputeRopeIndexBufferJob
            {
                resolutions = m_resolutions,
                ringCounts = m_vertexCounts,
                vertexOffsets = m_compactedVertexIndices,
                indexOffsets = m_compactedIndexIndices,
                indexBuffer = m_indexBuffer,
            };

            m_resourcesCreated = true;
        }

        void DisposeResources()
        {
            if (!m_resourcesCreated)
            {
                return;
            }

            if (m_mesh != null)
            {
                if (Application.isPlaying)
                {
                    Destroy(m_mesh);
                }
                else
                {
                    DestroyImmediate(m_mesh);
                }
            }

            m_resolutions.Dispose();
            m_radii.Dispose();
            m_vertexIndices.Dispose();
            m_vertexCounts.Dispose();
            m_featureIndices.Dispose();
            m_featureCounts.Dispose();
            m_twistHints.Dispose();
            m_compactedVertexIndices.Dispose();
            m_compactedIndexIndices.Dispose();

            m_featureTypes.Dispose();
            m_featureDataIndices.Dispose();
            m_featureTransformIndices.Dispose();
            
            m_pointData.Dispose();

            m_slackData.Dispose();
            
            m_obstacleData.Dispose();
            m_obstaclePositions.Dispose();
            m_obstacleTransforms.Dispose();
            
            m_sheeveData.Dispose();

            m_featureTransforms.Dispose();
            m_transforms.Dispose();

            m_vertices.Dispose();

            m_vertexCount.Dispose();
            m_indexCount.Dispose();
            
            m_vertexBuffer.Dispose();
            m_indexBuffer.Dispose();

            m_resourcesCreated = false;
        }
        
        [BurstCompile(DisableSafetyChecks = true, FloatMode = FloatMode.Fast)]
        struct UpdateFeatureDataJob : IJobParallelForTransform
        {
            public float4x4 worldToMeshLocalMatrix;
            public quaternion worldToMeshLocalRotation;
            
            [WriteOnly, NoAlias]
            public NativeArray<FeatureTransform> featureTransforms;

            /// <inheritdoc />
            public void Execute(int index, TransformAccess transform)
            {
                if (transform.isValid)
                {
                    FeatureTransform t;
                    t.position = math.transform(worldToMeshLocalMatrix, transform.position); 
                    t.rotation = worldToMeshLocalRotation * transform.rotation;
                    featureTransforms[index] = t;
                }
            }
        }
        
        [BurstCompile(DisableSafetyChecks = true, FloatMode = FloatMode.Fast, FloatPrecision = FloatPrecision.Low)]
        struct ComputeRopeVerticesJob : IJobParallelFor
        {
            [ReadOnly, NoAlias]
            public NativeArray<int> outputIndices;
            [ReadOnly, NoAlias]
            public NativeArray<int> featureIndices;
            [ReadOnly, NoAlias]
            public NativeArray<int> featureCounts;
            [ReadOnly, NoAlias]
            public NativeArray<FeatureType> featureTypes;
            [ReadOnly, NoAlias]
            public NativeArray<int> featureDataIndices;
            [ReadOnly, NoAlias]
            public NativeArray<int> featureTransformIndices;
            [ReadOnly, NoAlias]
            public NativeArray<PointData> pointData;
            [ReadOnly, NoAlias]
            public NativeArray<SlackData> slackData;
            [ReadOnly, NoAlias]
            public NativeArray<ObstacleData> obstacleData;
            [ReadOnly, NoAlias]
            public NativeArray<SheeveData> sheeveData;
            [ReadOnly, NoAlias]
            public NativeArray<FeatureTransform> featureTransforms;
            
            [NativeDisableParallelForRestriction, WriteOnly, NoAlias]
            public NativeArray<float3> obstaclePositions;
            [NativeDisableParallelForRestriction, WriteOnly, NoAlias]
            public NativeArray<float3> twistHints;
            [NativeDisableParallelForRestriction, WriteOnly, NoAlias]
            public NativeArray<int> vertexCounts;
            [NativeDisableParallelForRestriction, WriteOnly, NoAlias]
            public NativeArray<float3> vertices;
            
            public void Execute(int index)
            {
                var featureCount = featureCounts[index];
                var featureIndexStart = featureIndices[index];
                var featureIndexEnd = featureIndexStart + featureCount;

                var verticesIndexStart = outputIndices[index];
                var verticesIndex = verticesIndexStart;
                var prevVertex = float3.zero;
                
                for (var i = featureIndexStart; i < featureIndexEnd; i++)
                {
                    var featureType = featureTypes[i];
                    var featureDataIndex = featureDataIndices[i];
                    var featureTransformIndex = featureTransformIndices[i];
                    
                    var featureTransform = featureTransforms[featureTransformIndex];
                    var rotation = featureTransform.rotation;
                    var position = featureTransform.position;

                    switch (featureType)
                    {
                        case FeatureType.Point:
                        {
                            var point = pointData[featureDataIndex];
                            prevVertex = position + math.mul(rotation, point.localOffset);
                            vertices[verticesIndex++] = prevVertex;
                            break;
                        }
                        case FeatureType.Slack:
                        {
                            var slack = slackData[featureDataIndex];
                            var gravityDir = new float3(0f, -1f, 0f);

                            if (slack.slack < 0.001f)
                            {
                                break;
                            }
                            
                            var nextVertex = GetVertexApprox(prevVertex, i + 1);
                            
                            // project into 2d plane to simplify math
                            var prevToNextDisp = nextVertex - prevVertex;
                            var yv = math.project(prevToNextDisp, gravityDir);
                            var xv = prevToNextDisp - yv;
                            var x = math.length(xv);
                            var sag = x / slack.slack;
                            var xOffset = 0.5f * x;
                            var yOffset = mathExt.catenary(sag, xOffset);
                            var startVertex = prevVertex;
                            
                            for (var j = 0; j < slack.resolution; j++)
                            {
                                var fac = (float)(j + 1) / (slack.resolution + 1);
                                var offset = gravityDir * -(mathExt.catenary(sag, (x * fac) - xOffset) - yOffset);

                                prevVertex = startVertex + (prevToNextDisp * fac) + offset;
                                vertices[verticesIndex++] = prevVertex;
                            }
                            break;
                        }
                        case FeatureType.Obstacle:
                        {
                            var obstacle = obstacleData[featureDataIndex];
                            var edgeStart = position + math.mul(rotation, obstacle.edgeStart);
                            var edgeEnd = position + math.mul(rotation, obstacle.edgeEnd);
                            var edgeNormal = math.mul(rotation, obstacle.edgeNormal);

                            var nextVertex = GetVertexApprox(prevVertex, i + 1);

                            ClosestPointsOnLines(
                                prevVertex,
                                nextVertex,
                                edgeStart,
                                edgeEnd,
                                out var a,
                                out var vertex
                            );

                            var isBlocking = math.dot(vertex - a, edgeNormal) > 0f;
                            
                            if (obstacle.outputIndex >= 0)
                            {
                                obstaclePositions[obstacle.outputIndex] = isBlocking ? vertex : prevVertex;
                            }
                            if (isBlocking)
                            {
                                vertices[verticesIndex++] = vertex;
                                prevVertex = vertex;
                            }
                            break;
                        }
                        case FeatureType.Sheeve:
                        {
                            var sheeve = sheeveData[featureDataIndex];
                            var center = position + math.mul(rotation, sheeve.localOffset);
                            
                            var nextVertex = GetVertexApprox(center, i + 1);

                            // project everything to the plane the sheeve is on, which simplifies the math
                            var inverseRotation = math.inverse(rotation);
                            
                            var centerToPrevDisp = math.mul(inverseRotation, prevVertex - center).zy;
                            var centerToNextDisp = math.mul(inverseRotation, nextVertex - center).zy;

                            var centerToPrevDist = math.length(centerToPrevDisp);
                            var centerToNextDist = math.length(centerToNextDisp);

                            var centerToPrevDir = centerToPrevDisp / centerToPrevDist;
                            var centerToNextDir = centerToNextDisp / centerToNextDist;

                            var radiusSq = sheeve.radius * sheeve.radius;
                            var prevSin = -math.sqrt((centerToPrevDist * centerToPrevDist) - radiusSq) / centerToPrevDist;
                            var nextSin = math.sqrt((centerToNextDist * centerToNextDist) - radiusSq) / centerToNextDist;

                            var prevCos = sheeve.radius / centerToPrevDist;
                            var nextCos = sheeve.radius / centerToNextDist;
                            
                            if (sheeve.invertDirection)
                            {
                                prevSin *= -1f;
                                nextSin *= -1f;
                            }
                            
                            // get the direction from the center of the sheeve to the tangent point by rotating
                            // the vectors to the external points
                            var prevTangentDir = new float2(
                                centerToPrevDir.x * prevCos - centerToPrevDir.y * prevSin,
                                centerToPrevDir.x * prevSin + centerToPrevDir.y * prevCos
                            );
                            var nextTangentDir = new float2(
                                centerToNextDir.x * nextCos - centerToNextDir.y * nextSin,
                                centerToNextDir.x * nextSin + centerToNextDir.y * nextCos
                            );
                            
                            var prevAngle = math.atan2(prevTangentDir.y, prevTangentDir.x);
                            var nextAngle = math.atan2(nextTangentDir.y, nextTangentDir.x);
                            
                            var angle = nextAngle - prevAngle;
                            var radiansPerVertex = sheeve.radiansPerVertex;
                            
                            // normalize the angle to [0,2pi]
                            if (angle < 0f)
                            {
                                angle += 2f * math.PI;
                            }
                            if (!sheeve.invertDirection)
                            {
                                angle = (2f * math.PI) - angle;
                                radiansPerVertex *= -1f;
                            }
                            // skip sheeves where the rope winds most of the way around the sheeve, this is not 
                            // possible physically
                            if (angle > (3f / 2f) * math.PI)
                            {
                                break;
                            }

                            var cosAngle = math.cos(radiansPerVertex);
                            var sinAngle = math.sin(radiansPerVertex);
                            var vertex = sheeve.radius * prevTangentDir;
                            
                            angle -= 0.01f * sheeve.radiansPerVertex;

                            for (var a = 0f; a < angle; a += sheeve.radiansPerVertex)
                            {
                                vertices[verticesIndex++] = center + math.mul(rotation, new float3(0f, vertex.y, vertex.x));
                                
                                var rotatedVertex = new float2(
                                    vertex.x * cosAngle - vertex.y * sinAngle,
                                    vertex.x * sinAngle + vertex.y * cosAngle
                                );

                                vertex = rotatedVertex;
                            }

                            vertex = sheeve.radius * nextTangentDir;
                            var endVertex = center + math.mul(rotation, new float3(0f, vertex.y, vertex.x));
                            vertices[verticesIndex++] = endVertex;
                            prevVertex = endVertex;
                            break;
                        }
                    }
                }

                var firstFeatureTransform = featureTransforms[featureTransformIndices[featureIndexStart]];
                
                twistHints[index] = math.mul(firstFeatureTransform.rotation, new float3(1f, 0f, 0f));
                vertexCounts[index] = verticesIndex - verticesIndexStart;
            }

            float3 GetVertexApprox(float3 prevVertex, int index)
            {
                var featureType = featureTypes[index];
                var featureDataIndex = featureDataIndices[index];
                var featureTransformIndex = featureTransformIndices[index];
                
                var featureTransform = featureTransforms[featureTransformIndex];
                var rotation = featureTransform.rotation;
                var position = featureTransform.position;

                switch (featureType)
                {
                    case FeatureType.Point:
                    {
                        var point = pointData[featureDataIndex];
                        return position + math.mul(rotation, point.localOffset);
                    }
                    case FeatureType.Slack:
                    {
                        var slack = slackData[featureDataIndex];
                        var gravityDir = new float3(0f, -1f, 0f);

                        var nextVertex = GetVertexApprox(prevVertex, index + 1);

                        if (slack.slack < 0.001f)
                        {
                            return nextVertex;
                        }
                        
                        // project into 2d plane to simplify math
                        var prevToNextDisp = nextVertex - prevVertex;
                        var yv = math.project(prevToNextDisp, gravityDir);
                        var xv = prevToNextDisp - yv;
                        var x = math.length(xv);
                        var sag = x / slack.slack;
                        var xOffset = 0.5f * x;

                        var dir = math.normalize(
                            (0.01f * prevToNextDisp) -
                            (gravityDir * (mathExt.catenary(sag, (x * 0.01f) - xOffset) - mathExt.catenary(sag, -xOffset)))
                        );
                        
                        return prevVertex + (dir * 100f);
                    }
                    case FeatureType.Obstacle:
                    {
                        var obstacle = obstacleData[featureDataIndex];
                        var edgeStart = position + math.mul(rotation, obstacle.edgeStart);
                        var edgeEnd = position + math.mul(rotation, obstacle.edgeEnd);
                        var edgeNormal = math.mul(rotation, obstacle.edgeNormal);

                        var nextVertex = GetVertexApprox(prevVertex, index + 1);

                        ClosestPointsOnLines(
                            prevVertex,
                            nextVertex,
                            edgeStart,
                            edgeEnd,
                            out var a,
                            out var vertex
                        );

                        return math.dot(vertex - a, edgeNormal) > 0f ? vertex : nextVertex;
                    }
                    case FeatureType.Sheeve:
                    {
                        var sheeve = sheeveData[featureDataIndex];
                        var center = position + math.mul(rotation, sheeve.localOffset);

                        // project everything to the plane the sheeve is on, which simplifies the math
                        var inverseRotation = math.inverse(rotation);
                        var centerToPrevDisp = math.mul(inverseRotation, prevVertex - center).zy;
                        var centerToPrevDir = math.normalize(centerToPrevDisp);
                        
                        // Rotate the rope direction by 90 degrees towards the side of the sheeve the rope reeves
                        // through and pick the vertex in that direction. This approximation works well when the
                        // distance to the previous vertex is large.
                        var vertex = sheeve.radius * new float2(centerToPrevDir.y, -centerToPrevDir.x);
                        
                        if (sheeve.invertDirection)
                        {
                            vertex *= -1f;
                        }
                        
                        return center + math.mul(rotation, new float3(0f, vertex.y, vertex.x));
                    }
                    default:
                    {
                        return position;
                    }
                }
            }

            static float ClosestPointsOnLines(float3 a0, float3 a1, float3 b0, float3 b1, out float3 a2, out float3 b2)
            {
                var av = a1 - a0;
                var bv = b1 - b0;

                var aLen = math.length(av);
                var bLen = math.length(bv);

                var a = av / aLen;
                var b = bv / bLen;

                var cross = math.cross(a, b);
                var denom = math.lengthsq(cross);
                
                // lines are parallel
                if (math.abs(denom) < 0.0001f)
                {
                    var d0 = math.dot(a, b0 - a0);
                    var d1 = math.dot(a, b1 - a0);
                    
                    // segment B before A
                    if (d0 <= 0 && 0 >= d1) 
                    {
                        a2 = a0;
                        b2 = math.abs(d0) < math.abs(d1) ? b0 : b1;
                        return math.distance(a2, b2);
                    }
                    
                    // segment B after A
                    if (d0 >= aLen && aLen <= d1)
                    {
                        a2 = a1;
                        b2 = math.abs(d0) < math.abs(d1) ? b0 : b1;
                        return math.distance(a2, b2);
                    }
                    
                    // overlap
                    a2 = a1;
                    b2 = b1;
                    return math.distance((d0 * a) + a0, b0);
                }

                var t = b0 - a0;
                var detA = t.x * ((b.y * cross.z) - (cross.y * b.z)) - t.y * (b.x * cross.z - cross.x * b.z) + t.z * (b.x * cross.y - cross.x * b.y);
                var detB = t.x * ((a.y * cross.z) - (cross.y * a.z)) - t.y * (a.x * cross.z - cross.x * a.z) + t.z * (a.x * cross.y - cross.x * a.y);

                var t0 = detA / denom;
                var t1 = detB / denom;

                a2 = a0 + (a * t0);
                b2 = b0 + (b * t1);

                return math.distance(a2, b2);
            }
        }

        [BurstCompile]
        struct WritePositionJob : IJobParallelForTransform
        {
            [ReadOnly, NoAlias]
            public NativeArray<float3> positions;
            
            public void Execute(int index, TransformAccess transform)
            {
                transform.localPosition = positions[index];
            }
        }
        
        [BurstCompile(DisableSafetyChecks = true)]
        struct CompactRopeIndicesJob : IJob
        {
            public int ropeCount;
            [ReadOnly, NoAlias]
            public NativeArray<int> resolutions;
            [ReadOnly, NoAlias]
            public NativeArray<int> ringCounts;
            
            [WriteOnly, NoAlias]
            public NativeArray<int> vertexCount;
            [WriteOnly, NoAlias]
            public NativeArray<int> indexCount;
            [WriteOnly, NoAlias]
            public NativeArray<int> vertexOffsets;
            [WriteOnly, NoAlias]
            public NativeArray<int> indexOffsets;
            
            /// <inheritdoc />
            public void Execute()
            {
                var vertexOffset = 0;
                var indexOffset = 0;
                
                for (var i = 0; i < ropeCount; i++)
                {
                    vertexOffsets[i] = vertexOffset;
                    indexOffsets[i] = indexOffset;

                    var resolution = resolutions[i];
                    var ringCount = ringCounts[i];

                    var verticesPerRing = resolution + 1;
                    var indicesPerRing = 6 * resolution;

                    vertexOffset += ringCount * verticesPerRing;
                    indexOffset += (ringCount - 1) * indicesPerRing;
                }

                vertexCount[0] = vertexOffset;
                indexCount[0] = indexOffset;
            }
        }

        [BurstCompile(DisableSafetyChecks = true, FloatMode = FloatMode.Fast, FloatPrecision = FloatPrecision.Low)]
        struct ComputeRopeVertexBufferJob : IJobParallelFor
        {
            [ReadOnly, NoAlias]
            public NativeArray<int> resolutions;
            [ReadOnly, NoAlias]
            public NativeArray<float> radii;
            [ReadOnly, NoAlias]
            public NativeArray<float3> twistHints;
            [ReadOnly, NoAlias]
            public NativeArray<int> ringCounts;
            [ReadOnly, NoAlias]
            public NativeArray<int> ringOffsets;
            [ReadOnly, NoAlias]
            public NativeArray<float3> ringPositions;
            [ReadOnly, NoAlias]
            public NativeArray<int> vertexOffsets;
            
            [NativeDisableParallelForRestriction, WriteOnly, NoAlias]
            public NativeArray<Vertex32> vertexBuffer;

            /// <inheritdoc />
            public void Execute(int index)
            {
                var resolution = resolutions[index];
                var radius = radii[index];
                var twistHint = twistHints[index];
                var ringCount = ringCounts[index];
                var ringOffset = ringOffsets[index];
                var vertexOffset = vertexOffsets[index];

                var ringIndexStart = ringOffset;
                var ringIndexEnd = ringIndexStart + ringCount;
                var verticesPerRing = resolution + 1; 
                var radiansPerVertex = 2f * math.PI / resolution;
                var uvScaleX = 0.044f * radiansPerVertex;
                var uvScaleY = 0.05f / radius;
                var sin = math.sin(radiansPerVertex);
                var cos = math.cos(radiansPerVertex);

                var cumulativeLength = 0f;
                var lastDisplacement = float3.zero;
                var lastRotation = quaternion.identity;
                
                for (var i = ringIndexStart; i < ringIndexEnd; i++)
                {
                    var ringPosition = ringPositions[i];
                    var ringUvPosY = uvScaleY * cumulativeLength;
                    
                    quaternion ringRotation;
                    
                    if (i == (ringIndexEnd - 1))
                    {
                        ringRotation = lastRotation;
                    }
                    else
                    {
                        var nextRingPosition = ringPositions[i + 1];
                        var nextDisplacement = nextRingPosition - ringPosition;

                        cumulativeLength += math.length(nextDisplacement);

                        if (i == ringIndexStart)
                        {
                            ringRotation = quaternion.LookRotation(math.normalize(nextDisplacement), twistHint);
                            lastRotation = ringRotation;
                        }
                        else
                        {
                            var deltaRotation = mathExt.fromToRotation(lastDisplacement, nextDisplacement);
                            var nextRotation = math.mul(deltaRotation, lastRotation);
                        
                            ringRotation = math.slerp(nextRotation, lastRotation, 0.5f);
                            lastRotation = nextRotation;
                        }
                        
                        lastDisplacement = nextDisplacement;
                    }

                    var dir = new float3(1f, 0f, 0f);
                    
                    for (var j = 0; j < verticesPerRing; j++)
                    {
                        var position = ringPosition + math.mul(ringRotation, radius * dir);
                        var normal = math.mul(ringRotation, dir);
                        var tangent = math.mul(ringRotation, new float3(-dir.y, dir.x, 0f));
                        var uv = new float2(uvScaleX * j, ringUvPosY);

                        // write the vertex
                        var nrm = math.clamp(
                            (normal * sbyte.MaxValue) + math.select(-0.5f, 0.5f, normal > 0f),
                            sbyte.MinValue,
                            sbyte.MaxValue
                        );
                        var tan = math.clamp(
                            (tangent * sbyte.MaxValue) + math.select(-0.5f, 0.5f, tangent > 0f),
                            sbyte.MinValue,
                            sbyte.MaxValue
                        );
                        
                        Vertex32 vert;
                        vert.pos = new float4(position, 1f);
                        vert.nrmX = (sbyte)nrm.x;
                        vert.nrmY = (sbyte)nrm.y;
                        vert.nrmZ = (sbyte)nrm.z;
                        vert.nrmW = 0;
                        vert.tanX = (sbyte)tan.x;
                        vert.tanY = (sbyte)tan.y;
                        vert.tanZ = (sbyte)tan.z;
                        vert.tanW = 127; // 1
                        vert.uv = uv;
                        
                        vertexBuffer[vertexOffset++] = vert;
                        
                        // rotate the vector around z axis
                        var rotatedDir = new float2(
                            dir.x * cos - dir.y * sin,
                            dir.x * sin + dir.y * cos
                        );

                        dir.xy = rotatedDir.xy;
                    }
                }
            }
        }

        [BurstCompile(DisableSafetyChecks = true)]
        struct ComputeRopeIndexBufferJob : IJobParallelFor
        {
            [ReadOnly, NoAlias]
            public NativeArray<int> resolutions;
            [ReadOnly, NoAlias]
            public NativeArray<int> ringCounts;
            [ReadOnly, NoAlias]
            public NativeArray<int> vertexOffsets;
            [ReadOnly, NoAlias]
            public NativeArray<int> indexOffsets;
            
            [NativeDisableParallelForRestriction, WriteOnly, NoAlias]
            public NativeArray<ushort> indexBuffer;

            /// <inheritdoc />
            public void Execute(int index)
            {
                var resolution = resolutions[index];
                var ringCount = ringCounts[index];
                var vertexOffset = vertexOffsets[index];
                var indexOffset = indexOffsets[index];

                var verticesPerRing = resolution + 1;
                
                 for (var i = 0; i < ringCount - 1; i++)
                 {
                     for (var j = 0; j < resolution; j++)
                     {
                         var vertA = vertexOffset;
                         var vertC = vertexOffset + verticesPerRing;
                         var vertB = vertA + 1;
                         var vertD = vertC + 1;
                         
                         indexBuffer[indexOffset + 0] = (ushort)vertA;
                         indexBuffer[indexOffset + 1] = (ushort)vertB;
                         indexBuffer[indexOffset + 2] = (ushort)vertC;
                         indexBuffer[indexOffset + 3] = (ushort)vertC;
                         indexBuffer[indexOffset + 4] = (ushort)vertB;
                         indexBuffer[indexOffset + 5] = (ushort)vertD;
                
                         indexOffset += 6;
                         vertexOffset++;
                     }
                     
                     vertexOffset++;
                 }
            }
        }
    }
}
