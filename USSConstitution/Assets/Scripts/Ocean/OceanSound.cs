﻿using UnityEngine;

namespace Constitution
{
    public class OceanSound : MonoBehaviour
    {
        private void LateUpdate()
        {
            var camPos = Camera.main.transform.position;
            camPos.y = 0;
            transform.position = camPos;
        }
    }
}
