﻿using UnityEngine;

namespace Constitution
{
    public class Settings : MonoBehaviour
    {
        [SerializeField]
        bool m_showFps = true;

        readonly FrameTiming[] m_frameTimings = new FrameTiming[1];
        float m_cpuAvg = 0f;
        float m_gpuAvg = 0f;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                m_showFps = !m_showFps;
            }
            if (Input.GetKeyDown(KeyCode.F10))
            {
                QualitySettings.vSyncCount = QualitySettings.vSyncCount == 1 ? 2 : 1;
            }
            if (Input.GetKeyDown(KeyCode.F11))
            {
                Screen.fullScreen = !Screen.fullScreen;
            }
            
            if (m_showFps) 
            {
                FrameTimingManager.CaptureFrameTimings();
                FrameTimingManager.GetLatestTimings((uint)m_frameTimings.Length, m_frameTimings);
                m_cpuAvg = Mathf.Lerp(m_cpuAvg, (float)m_frameTimings[0].cpuFrameTime, 0.1f);
                m_gpuAvg = Mathf.Lerp(m_gpuAvg, (float)m_frameTimings[0].gpuFrameTime, 0.1f);
            }
        }

        void OnGUI()
        {
            if (m_showFps)
            {
                var text = $"cpu: {m_cpuAvg:n2}ms ({1000f / m_cpuAvg:n2} fps)\n" +
                           $"gpu: {m_gpuAvg:n2}ms ({1000f / m_gpuAvg:n2} fps)\n" +
                           $"\n" +
                           $"Fire Cannons:".PadRight(30) + $"J\n" +
                           $"Lamps:".PadRight(30) + $"L\n" +
                           $"Lamp Shadows:".PadRight(30) + $"O\n" +
                           $"Vsync 1-2 Toggle:".PadRight(30) + $"F10\n" +
                           $"Fullscreen Toggle:".PadRight(30) + $"F11\n";
                GUI.TextField(new Rect(10, 10, 200, 200), text);
            }
        }
    }
}
