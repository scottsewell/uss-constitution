﻿using UnityEngine;

namespace Constitution
{
    public class Gun : MonoBehaviour
    {
        [Header("Firing")]

        [SerializeField]
        [Tooltip("The projectile shot by this gun.")]
        private Projectile m_projectile = null;

        [SerializeField]
        [Tooltip("The transform the projectile is spawned at.")]
        private Transform m_muzzle = null;

        [SerializeField]
        [Range(0f, 1f)]
        private float m_fireTimeVariance = 0.5f;

        [SerializeField]
        [Range(0f, 5f)]
        private float m_recoilTime = 1f;

        [SerializeField]
        [Range(0f, 60f)]
        private float m_reloadTime = 10f;

        [SerializeField]
        [Range(0f, 5f)]
        private float m_reloadVariance = 1f;

        [SerializeField]
        [Range(0f, 20f)]
        private float m_runoutTime = 5f;

        [SerializeField]
        [Range(0f, 10f)]
        private float m_runoutVariance = 2f;

        [SerializeField]
        private ParticleSystem m_fireParticles = null;

        [SerializeField]
        private AudioConfigPlayer m_fireAudio = null;
        [SerializeField]
        private AudioConfigPlayer m_recoilAudio = null;
        [SerializeField]
        private AudioSource m_runoutAudio = null;

        private Animator m_anim = null;
        private bool m_firing = false;
        private bool m_firedShot = false;
        private float m_fireTime = 0f;
        private float m_reloadDuration = 0f;
        private float m_runoutDuration = 0f;
        private float m_recoil = 0f;

        /// <summary>
        /// Is the gun able to fire right now.
        /// </summary>
        public bool CanFire => !m_firing;

        protected virtual void Awake()
        {
            m_anim = GetComponentInChildren<Animator>();
        }

        protected virtual void Update()
        {
            if (m_firing)
            {
                var timeSinceFire = Time.time - m_fireTime;

                if (timeSinceFire >= 0 && !m_firedShot)
                {
                    if (m_projectile && m_muzzle)
                    {
                        Instantiate(m_projectile, m_muzzle.position, m_muzzle.rotation);
                    }
                    if (m_fireParticles)
                    {
                        m_fireParticles.Play();
                    }
                    m_fireAudio.Play();
                    m_recoilAudio.Play();
                    m_firedShot = true;
                }

                var timeSinceRunout = timeSinceFire - m_reloadDuration;

                if (timeSinceRunout < 0)
                {
                    m_recoil = Mathf.Clamp(0.5f * timeSinceFire / m_recoilTime, 0.0f, 0.5f);
                }
                else
                {
                    if (timeSinceRunout - m_runoutDuration > 0)
                    {
                        m_firing = false;
                    }
                    m_recoil = Mathf.Clamp(0.5f * timeSinceRunout / m_runoutDuration, 0.0f, 0.5f) + 0.5f;
                }

                if (0.5f < m_recoil && m_recoil < 1.0f)
                {
                    if (!m_runoutAudio.isPlaying)
                    {
                        m_runoutAudio.Play();
                    }

                    const float fade = 0.1f;
                    m_runoutAudio.volume = Mathf.Min(Mathf.Min(m_recoil - 0.5f, 1f - m_recoil) / fade, 1f);
                }
                else if (m_runoutAudio.isPlaying)
                {
                    m_runoutAudio.Stop();
                }
            }

            m_anim.Play("Recoil", 0, m_recoil);
        }

        public void Fire(float delay)
        {
            if (CanFire)
            {
                m_firing = true;
                m_fireTime = Time.time + delay + Random.Range(0f, m_fireTimeVariance);
                m_reloadDuration = m_reloadTime + Random.Range(0f, m_reloadVariance);
                m_runoutDuration = m_runoutTime + Random.Range(0f, m_runoutVariance);
                m_firedShot = false;
            }
        }
    }
}
