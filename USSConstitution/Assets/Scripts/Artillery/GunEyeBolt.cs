﻿using UnityEngine;

namespace Constitution
{
    public class GunEyeBolt : MonoBehaviour
    {
        [SerializeField]
        Transform m_left;
        [SerializeField]
        Transform m_right;
        
        public Transform LeftEyebolt => m_left;
        public Transform RightEyebolt => m_right;
    }
}
