﻿using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

using UnityEngine;
using UnityEngine.Jobs;

namespace Constitution
{
    public class GunBlockPlacement : Constraint<GunBlockPlacement, PositionConstraintManager>
    {
        protected override PositionConstraintManager Manager => PositionConstraintManager.Instance;

        [SerializeField]
        bool m_IsLeft = false;

        public Transform Target { get; private set; }

        void Start()
        {
            var position = transform.position;
            var bestDistance = float.MaxValue;
            foreach (var bolt in transform.root.GetComponentsInChildren<GunEyeBolt>(true))
            {
                var target = m_IsLeft ? bolt.RightEyebolt : bolt.LeftEyebolt;
                var distance = Vector3.Distance(target.position, position);

                if (bestDistance > distance)
                {
                    bestDistance = distance;
                    Target = target;
                }
            }
        }
    }

    public class PositionConstraintManager : ConstraintManager<GunBlockPlacement, PositionConstraintManager>
    {
        public override int Priority => 50;
        
        TransformAccessArray m_targets;
        TransformAccessArray m_transforms;
        NativeArray<float3> m_positions;
        
        ReadPositionJob m_readPositionJob;
        WritePositionJob m_writePositionJob;

        protected override void OnDispose(bool disposing)
        {
            ClearBuffers();
        }

        void ClearBuffers()
        {
            if (m_targets.isCreated)
            {
                m_targets.Dispose();
                m_targets = default;
            }
            if (m_transforms.isCreated)
            {
                m_transforms.Dispose();
                m_transforms = default;
            }
            if (m_positions.IsCreated)
            {
                m_positions.Dispose();
                m_positions = default;
            }
        }

        protected override JobHandle Execute()
        {
            if (m_dirty)
            {
                ClearBuffers();

                var count = m_constraints.Count;
                m_targets = new TransformAccessArray(count);
                m_transforms = new TransformAccessArray(count);

                for (var i = 0; i < count; i++)
                {
                    var constraint = m_constraints[i];

                    m_targets.Add(constraint.Target);
                    m_transforms.Add(constraint.transform);
                }
                
                m_positions = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );

                m_readPositionJob = new ReadPositionJob
                {
                    positions = m_positions,
                };
                m_writePositionJob = new WritePositionJob
                {
                    positions = m_positions,
                };
                
                m_dirty = false;
            }

            var readJob = m_readPositionJob.ScheduleReadOnlyByRef(m_targets, 32);
            var writeJob = m_writePositionJob.ScheduleByRef(m_transforms, readJob);

            return writeJob;
        }

        [BurstCompile]
        struct ReadPositionJob : IJobParallelForTransform
        {
            [NativeDisableUnsafePtrRestriction, WriteOnly, NoAlias]
            public NativeArray<float3> positions;

            public void Execute(int index, TransformAccess transform)
            {
                positions[index] = transform.position;
            }
        }

        [BurstCompile]
        struct WritePositionJob : IJobParallelForTransform
        {
            [NativeDisableUnsafePtrRestriction, ReadOnly, NoAlias]
            public NativeArray<float3> positions;

            public void Execute(int index, TransformAccess transform)
            {
                transform.position = positions[index];
            }
        }
    }
}
