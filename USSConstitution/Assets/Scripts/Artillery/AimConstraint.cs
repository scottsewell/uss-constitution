﻿using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;

using UnityEngine;
using UnityEngine.Jobs;

namespace Constitution
{
    public class AimConstraint : Constraint<AimConstraint, AimConstraintManager>
    {
        protected override AimConstraintManager Manager => AimConstraintManager.Instance;
        
        [SerializeField]
        Transform m_target;
        [SerializeField]
        Vector3 m_upDir = Vector3.up;
        [SerializeField]
        Vector3 m_offset;

        public Transform Target => m_target;
        public Vector3 UpDir => m_upDir;
        public Vector3 Offset => m_offset;
    }
    
    public class AimConstraintManager : ConstraintManager<AimConstraint, AimConstraintManager>
    {
        public override int Priority => 100;

        TransformAccessArray m_transforms;
        TransformAccessArray m_targets;
        NativeArray<float3> m_upDirs;
        NativeArray<float3> m_offsets;
        NativeArray<float3> m_positions;
        NativeArray<quaternion> m_rotations;
        
        ReadPositionJob m_readPositionJob;
        AimJob m_aimJob;
        WriteRotationJob m_writeRotationJob;

        protected override void OnDispose(bool disposing)
        {
            ClearBuffers();
        }

        void ClearBuffers()
        {
            if (m_transforms.isCreated)
            {
                m_transforms.Dispose();
                m_transforms = default;
            }
            if (m_targets.isCreated)
            {
                m_targets.Dispose();
                m_targets = default;
            }
            if (m_upDirs.IsCreated)
            {
                m_upDirs.Dispose();
                m_upDirs = default;
            }
            if (m_offsets.IsCreated)
            {
                m_offsets.Dispose();
                m_offsets = default;
            }
            if (m_positions.IsCreated)
            {
                m_positions.Dispose();
                m_positions = default;
            }
            if (m_rotations.IsCreated)
            {
                m_rotations.Dispose();
                m_rotations = default;
            }
        }

        protected override JobHandle Execute()
        {
            if (m_dirty)
            {
                ClearBuffers();
                
                var count = m_constraints.Count;
                m_transforms = new TransformAccessArray(count);
                m_targets = new TransformAccessArray(count);

                m_upDirs = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                m_offsets = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                
                for (var i = 0; i < count; i++)
                {
                    var constraint = m_constraints[i];
                    
                    m_transforms.Add(constraint.transform);
                    m_targets.Add(constraint.Target);
                    m_upDirs[i] = constraint.UpDir;
                    m_offsets[i] = constraint.Offset;
                }
                
                m_positions = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                m_rotations = new NativeArray<quaternion>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                
                m_readPositionJob = new ReadPositionJob
                {
                    positions = m_positions,
                };
                m_aimJob = new AimJob
                {
                    upDirs = m_upDirs,
                    offsets = m_offsets,
                    positions = m_positions,
                    rotations = m_rotations,
                };
                m_writeRotationJob = new WriteRotationJob
                {
                    rotations = m_rotations,
                };

                m_dirty = false;
            }

            var readJob = m_readPositionJob.ScheduleReadOnlyByRef(m_targets, 32);
            var aimJob = m_aimJob.ScheduleReadOnlyByRef(m_transforms, 8, readJob);
            var writeJob = m_writeRotationJob.ScheduleByRef(m_transforms, aimJob);

            return writeJob;
        }

        [BurstCompile]
        struct ReadPositionJob : IJobParallelForTransform
        {
            [NativeDisableUnsafePtrRestriction, WriteOnly, NoAlias]
            public NativeArray<float3> positions;

            public void Execute(int index, TransformAccess transform)
            {
                if (transform.isValid)
                {
                    positions[index] = transform.position;
                }
            }
        }

        [BurstCompile]
        struct AimJob : IJobParallelForTransform
        {
            [NativeDisableUnsafePtrRestriction, ReadOnly, NoAlias]
            public NativeArray<float3> upDirs;
            [NativeDisableUnsafePtrRestriction, ReadOnly, NoAlias]
            public NativeArray<float3> offsets;
            [NativeDisableUnsafePtrRestriction, ReadOnly, NoAlias]
            public NativeArray<float3> positions;
            [NativeDisableUnsafePtrRestriction, WriteOnly, NoAlias]
            public NativeArray<quaternion> rotations;
            
            public void Execute(int index, TransformAccess transform)
            {
                if (transform.isValid)
                {
                    var dir = math.normalize((float3)transform.position - positions[index]);
                    var up = math.rotate(transform.rotation, math.normalize(upDirs[index]));
                    var lookRot = quaternion.LookRotation(dir, up);
                    var lookOffset = quaternion.Euler(math.radians(offsets[index]));
                
                    rotations[index] = math.mul(lookRot, lookOffset);
                }
            }
        }

        [BurstCompile]
        struct WriteRotationJob : IJobParallelForTransform
        {
            [NativeDisableUnsafePtrRestriction, ReadOnly, NoAlias]
            public NativeArray<quaternion> rotations;
            
            public void Execute(int index, TransformAccess transform)
            {
                transform.rotation = rotations[index];
            }
        }
    }
}
