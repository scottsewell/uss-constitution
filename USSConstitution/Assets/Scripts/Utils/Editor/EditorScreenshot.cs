﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Constitution
{
    /// <summary>
    /// Utility for capturing screenshots in the editor.
    /// </summary>
    public static class EditorScreenshot
    {
        static readonly string OUTPUT_DIRECTORY = "Screenshots/";

        static string m_latestScreenshot = "";

        [MenuItem("Screenshot/Take Screenshot _F10")]
        static void TakeScreenshot()
        {
            var directory = OUTPUT_DIRECTORY;

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var filename = $"Screenshot_{DateTime.Now:yyyy-MM-dd_HH-mm-ss}.png";
            var path = directory + filename;

            ScreenCapture.CaptureScreenshot(path);

            m_latestScreenshot = path;

            Debug.Log($"Screenshot saved: <b>\"{path}\"</b> with resolution <b>{GetResolution()}</b>");
        }

        [MenuItem("Screenshot/Reveal in Explorer")]
        static void ShowFolder()
        {
            if (File.Exists(m_latestScreenshot))
            {
                EditorUtility.RevealInFinder(m_latestScreenshot);
                return;
            }

            var directory = OUTPUT_DIRECTORY;

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            EditorUtility.RevealInFinder(directory);
        }

        //private static string GetResolution()
        //{
        //    var gameView = GetMainGameView();
        //    var prop = gameView.GetType().GetProperty("currentGameViewSize", BindingFlags.NonPublic | BindingFlags.Instance);
        //    var gvsize = prop.GetValue(gameView, new object[0] { });
        //    var gvSizeType = gvsize.GetType();

        //    var ScreenHeight = (int)gvSizeType.GetProperty("height", BindingFlags.Public | BindingFlags.Instance).GetValue(gvsize, new object[0] { });
        //    var ScreenWidth = (int)gvSizeType.GetProperty("width", BindingFlags.Public | BindingFlags.Instance).GetValue(gvsize, new object[0] { });

        //    return ScreenWidth.ToString() + "x" + ScreenHeight.ToString();
        //}

        //private static EditorWindow GetMainGameView()
        //{
        //    var type = Type.GetType("UnityEditor.GameView,UnityEditor");
        //    var getMainGameView = type.GetMethod("GetMainGameView", BindingFlags.NonPublic | BindingFlags.Static);
        //    return getMainGameView.Invoke(null, null) as EditorWindow;
        //}

        private static string GetResolution()
        {
            var size = Handles.GetMainGameViewSize();
            var sizeInt = new Vector2Int((int)size.x, (int)size.y);
            return $"{sizeInt.x}x{sizeInt.y}";
        }
    }
}