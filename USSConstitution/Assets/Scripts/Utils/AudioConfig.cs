﻿using System;

using UnityEngine;

using Framework;

using Random = UnityEngine.Random;

namespace Constitution
{
    [Serializable]
    public class AudioConfigPlayer
    {
        [SerializeField]
        [Tooltip("The audio clip to play.")]
        private AudioConfig m_config = null;

        [SerializeField]
        [Tooltip("The source to play using.")]
        private AudioSource m_source = null;

        /// <summary>
        /// Plays the audio.
        /// </summary>
        public void Play()
        {
            if (m_config != null)
            {
                m_config.Play(m_source);
            }
        }
    }

    [CreateAssetMenu(fileName = "New AudioConfig", menuName = "Constitution/Audio/Config")]
    public class AudioConfig : ScriptableObject
    {
        [SerializeField]
        [Tooltip("The clips to randomly pick from when this is played.")]
        private AudioClip[] m_clips = null;

        [SerializeField]
        [Tooltip("The range of the random pitch when played.")]
        [MinMaxSlider(0f, 2f)]
        private MinMaxRange m_pitch = new MinMaxRange(1f, 1f);

        [SerializeField]
        [Tooltip("The range of the random volume when played.")]
        [MinMaxSlider(0f, 1f)]
        private MinMaxRange m_volume = new MinMaxRange(1f, 1f);

        /// <summary>
        /// Plays the audio using the given audio source.
        /// </summary>
        /// <param name="source">The source to play using.</param>
        public void Play(AudioSource source)
        {
            if (source != null)
            {
                source.pitch = Random.Range(m_pitch.Min, m_pitch.Max);
                source.PlayOneShot(m_clips.PickRandom(), Random.Range(m_volume.Min, m_volume.Max));
            }
        }
    }
}
