﻿using System;
using System.Collections.Generic;
using System.Linq;

using Framework;

using Unity.Collections;
using Unity.Jobs;

using UnityEngine;

namespace Constitution
{
    interface IConstraintManager : IDisposable
    {
        int Priority { get; }
        bool IsActive { get; }
        JobHandle Execute();
    }

    public abstract class ConstraintManager<T, TManager> : Disposable, IConstraintManager
        where T : Constraint<T, TManager>
        where TManager : ConstraintManager<T, TManager>, new()
    {
        public static TManager Instance { get; private set; }

        protected ConstraintManager()
        {
            Instance = this as TManager;
        }
        
        protected readonly List<T> m_constraints = new List<T>();
        protected bool m_dirty;

        public virtual int Priority => 0;

        bool IConstraintManager.IsActive => m_constraints.Count > 0;

        internal void Register(T constraint)
        {
            if (constraint != null && !m_constraints.Contains(constraint))
            {
                m_constraints.Add(constraint);
                m_dirty = true;
            }
        }

        internal void Deregister(T constraint)
        {
            if (constraint != null && m_constraints.Remove(constraint))
            {
                m_dirty = true;
            }
        }

        internal void MarkDirty()
        {
            m_dirty = true;
        }

        JobHandle IConstraintManager.Execute() => Execute();
        protected abstract JobHandle Execute();
    }

    static class ConstraintManager
    {
        static IConstraintManager[] m_constraints;

        static ConstraintManager()
        {
            Application.quitting += OnQuit;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        static void Init()
        {
            // In case the domain reload on enter play mode is disabled, we must
            // reset all static fields.
            DisposeConstraints();

            // create the constraint managers for each type of constraint
            m_constraints = ReflectionUtils.GetTypesDerivedFrom<IConstraintManager>()
                .Where(type => !type.IsAbstract)
                .Select(type => Activator.CreateInstance(type))
                .Cast<IConstraintManager>()
                .OrderBy(constraint => constraint.Priority)
                .ToArray();
        }

        static void OnQuit()
        {
            DisposeConstraints();
        }

        static void DisposeConstraints()
        {
            if (m_constraints != null)
            {
                foreach (var constraint in m_constraints)
                {
                    constraint.Dispose();
                }
                m_constraints = null;
            }
        }

        internal static void Update()
        {
            if (m_constraints == null)
            {
                return;
            }
            
            var jobs = new NativeArray<JobHandle>(
                m_constraints.Length,
                Allocator.Temp,
                NativeArrayOptions.UninitializedMemory
            );

            var count = 0;
            for (var i = 0; i < m_constraints.Length; i++)
            {
                var constraint = m_constraints[i];
                
                if (constraint.IsActive)
                {
                    jobs[count++] = constraint.Execute();
                }
            }

            var waitHandle = JobHandle.CombineDependencies(jobs.Slice(0, count));
            waitHandle.Complete();
            
            jobs.Dispose();
        }
    }
}
