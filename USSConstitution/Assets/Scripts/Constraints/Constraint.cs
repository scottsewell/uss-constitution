﻿using UnityEngine;

namespace Constitution
{
    public abstract class Constraint<T, TManager> : MonoBehaviour
        where T : Constraint<T, TManager>
        where TManager : ConstraintManager<T, TManager>, new()
    {
        protected abstract TManager Manager { get; }

        protected virtual void OnValidate()
        {
            if (Manager != null)
            {
                Manager.MarkDirty();
            }
        }
        
        protected virtual void OnEnable()
        {
            if (Manager != null)
            {
                Manager.Register(this as T);
            }
        }

        protected virtual void OnDisable()
        {
            if (Manager != null)
            {
                Manager.Deregister(this as T);
            }
        }
    }
}
