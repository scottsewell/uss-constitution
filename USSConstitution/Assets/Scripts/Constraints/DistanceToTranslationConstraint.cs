﻿using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

using UnityEngine;
using UnityEngine.Jobs;

namespace Constitution
{
    public class DistanceToTranslationConstraint : Constraint<DistanceToTranslationConstraint, DistanceToTranslationConstraintManager>
    {
        protected override DistanceToTranslationConstraintManager Manager => DistanceToTranslationConstraintManager.Instance;

        [SerializeField]
        Vector3 m_direction = Vector3.up;
        [SerializeField]
        float m_offset = 0f;
        [SerializeField, Range(0.1f, 10f)]
        float m_scale = 1f;
        [SerializeField]
        Transform m_sourceA;
        [SerializeField]
        Transform m_sourceB;

        public Vector3 Direction => m_direction.normalized;
        public float Offset => m_offset;
        public float Scale => m_scale;
        public Transform SourceA => m_sourceA;
        public Transform SourceB => m_sourceB;
    }
    
    public class DistanceToTranslationConstraintManager : ConstraintManager<DistanceToTranslationConstraint, DistanceToTranslationConstraintManager>
    {
        TransformAccessArray m_transforms;
        TransformAccessArray m_sourcesA;
        TransformAccessArray m_sourcesB;
        NativeArray<float3> m_directions;
        NativeArray<float> m_offsets;
        NativeArray<float> m_scales;
        NativeArray<float3> m_sourcePositionsA;
        NativeArray<float3> m_sourcePositionsB;
        NativeArray<float3> m_newPositions;
        
        ReadPositionJob m_readPositionAJob;
        ReadPositionJob m_readPositionBJob;
        DistanceJob m_distanceJob;
        WritePositionJob m_writePositionJob;

        protected override void OnDispose(bool disposing)
        {
            ClearBuffers();
        }

        void ClearBuffers()
        {
            if (m_transforms.isCreated)
            {
                m_transforms.Dispose();
                m_transforms = default;
            }
            if (m_sourcesA.isCreated)
            {
                m_sourcesA.Dispose();
                m_sourcesA = default;
            }
            if (m_sourcesB.isCreated)
            {
                m_sourcesB.Dispose();
                m_sourcesB = default;
            }
            if (m_directions.IsCreated)
            {
                m_directions.Dispose();
                m_directions = default;
            }
            if (m_offsets.IsCreated)
            {
                m_offsets.Dispose();
                m_offsets = default;
            }
            if (m_scales.IsCreated)
            {
                m_scales.Dispose();
                m_scales = default;
            }
            if (m_sourcePositionsA.IsCreated)
            {
                m_sourcePositionsA.Dispose();
                m_sourcePositionsA = default;
            }
            if (m_sourcePositionsB.IsCreated)
            {
                m_sourcePositionsB.Dispose();
                m_sourcePositionsB = default;
            }
            if (m_newPositions.IsCreated)
            {
                m_newPositions.Dispose();
                m_newPositions = default;
            }
        }

        protected override JobHandle Execute()
        {
            if (m_dirty)
            {
                ClearBuffers();
                
                var count = m_constraints.Count;
                m_transforms = new TransformAccessArray(count);
                m_sourcesA = new TransformAccessArray(count);
                m_sourcesB = new TransformAccessArray(count);

                m_directions = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                m_offsets = new NativeArray<float>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                m_scales = new NativeArray<float>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                
                for (var i = 0; i < count; i++)
                {
                    var constraint = m_constraints[i];
                    m_transforms.Add(constraint.transform);
                    m_sourcesA.Add(constraint.SourceA);
                    m_sourcesB.Add(constraint.SourceB);
                    m_directions[i] = constraint.Direction;
                    m_offsets[i] = constraint.Offset;
                    m_scales[i] = constraint.Scale;
                }
                
                m_sourcePositionsA = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                m_sourcePositionsB = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                m_newPositions = new NativeArray<float3>(
                    count,
                    Allocator.Persistent,
                    NativeArrayOptions.UninitializedMemory
                );
                
                m_readPositionAJob = new ReadPositionJob
                {
                    positions = m_sourcePositionsA,
                };
                m_readPositionBJob = new ReadPositionJob
                {
                    positions = m_sourcePositionsB,
                };
                m_distanceJob = new DistanceJob
                {
                    directions = m_directions,
                    offsets = m_offsets,
                    scales = m_scales,
                    sourcePositionsA = m_sourcePositionsA,
                    sourcePositionsB = m_sourcePositionsB,
                    newPositions = m_newPositions,
                };
                m_writePositionJob = new WritePositionJob
                {
                    positions = m_newPositions,
                };

                m_dirty = false;
            }

            var readAJob = m_readPositionAJob.ScheduleReadOnlyByRef(m_sourcesA, 32);
            var readBJob = m_readPositionBJob.ScheduleReadOnlyByRef(m_sourcesB, 32);
            var readJob = JobHandle.CombineDependencies(readAJob, readBJob);
            var distanceJob = m_distanceJob.ScheduleByRef(m_transforms.length, 32, readJob);
            var writeJob = m_writePositionJob.ScheduleByRef(m_transforms, distanceJob);

            return writeJob;
        }

        [BurstCompile]
        struct ReadPositionJob : IJobParallelForTransform
        {
            [WriteOnly, NoAlias]
            public NativeArray<float3> positions;

            public void Execute(int index, TransformAccess transform)
            {
                if (transform.isValid)
                {
                    positions[index] = transform.position;
                }
            }
        }

        [BurstCompile]
        struct DistanceJob : IJobParallelFor
        {
            [ReadOnly, NoAlias]
            public NativeArray<float3> directions;
            [ReadOnly, NoAlias]
            public NativeArray<float> offsets;
            [ReadOnly, NoAlias]
            public NativeArray<float> scales;
            [ReadOnly, NoAlias]
            public NativeArray<float3> sourcePositionsA;
            [ReadOnly, NoAlias]
            public NativeArray<float3> sourcePositionsB;
            [WriteOnly, NoAlias]
            public NativeArray<float3> newPositions;
            
            public void Execute(int index)
            {
                var posA = sourcePositionsA[index];
                var posB = sourcePositionsB[index];
                var distance = math.distance(posA, posB) - offsets[index];
            
                newPositions[index] = scales[index] * distance * directions[index];
            }
        }

        [BurstCompile]
        struct WritePositionJob : IJobParallelForTransform
        {
            [ReadOnly, NoAlias]
            public NativeArray<float3> positions;
            
            public void Execute(int index, TransformAccess transform)
            {
                transform.localPosition = positions[index];
            }
        }
    }
}
