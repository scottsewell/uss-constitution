%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Carronade_AnimMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Carronade_Rig
    m_Weight: 1
  - m_Path: Carronade_Rig/Root
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/ScrewTarget
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/ScrewTarget/ScrewTarget_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L/BreachRope4.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L/BreachRope4.L/BreachRope5.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L/BreachRope4.L/BreachRope5.L/BreachRope5b.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L/BreachRope4.L/BreachRope5.L/BreachRope5b.L/BreachRope6.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L/BreachRope4.L/BreachRope5.L/BreachRope5b.L/BreachRope6.L/BreachRope7.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L/BreachRope4.L/BreachRope5.L/BreachRope5b.L/BreachRope6.L/BreachRope7.L/BreachRope8.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.L/BreachRope2.L/BreachRope3.L/BreachRope4.L/BreachRope5.L/BreachRope5b.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope8.L_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R/BreachRope4.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R/BreachRope4.R/BreachRope5.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R/BreachRope4.R/BreachRope5.R/BreachRope5b.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R/BreachRope4.R/BreachRope5.R/BreachRope5b.R/BreachRope6.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R/BreachRope4.R/BreachRope5.R/BreachRope5b.R/BreachRope6.R/BreachRope7.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R/BreachRope4.R/BreachRope5.R/BreachRope5b.R/BreachRope6.R/BreachRope7.R/BreachRope8.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/Cannon/BreachRope1.R/BreachRope2.R/BreachRope3.R/BreachRope4.R/BreachRope5.R/BreachRope5b.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope8.R_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/ScrewAngle
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/ScrewAngle/Screw
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/ScrewAngle/Screw/Screw_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/SledRope1.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/SledRope1.L/SledRope2.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/SledRope1.L/SledRope2.L/SledRope2.L_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/SledRope1.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/SledRope1.R/SledRope2.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/Sled/SledRope1.R/SledRope2.R/SledRope2.R_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/MainRope1.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/MainRope1.L/MainRope2.L
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/MainRope1.L/MainRope2.L/MainRope2.L_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/MainRope1.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/MainRope1.R/MainRope2.R
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/Main/MainRope1.R/MainRope2.R/MainRope2.R_end
    m_Weight: 1
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.L
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.L/MainRope8.L
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.L/MainRope8.L/MainRope7.L
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.L/MainRope8.L/MainRope7.L/MainRope7.L_end
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.R
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.R/MainRope8.R
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.R/MainRope8.R/MainRope7.R
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/MainRopeAnchor.R/MainRope8.R/MainRope7.R/MainRope7.R_end
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.L
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.L/SledRope8.L
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.L/SledRope8.L/SledRope7.L
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.L/SledRope8.L/SledRope7.L/SledRope7.L_end
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.R
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.R/SledRope8.R
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.R/SledRope8.R/SledRope7.R
    m_Weight: 0
  - m_Path: Carronade_Rig/Root/SledRopeAnchor.R/SledRope8.R/SledRope7.R/SledRope7.R_end
    m_Weight: 0
  - m_Path: Carronade_Rig/BreachRope9.L
    m_Weight: 1
  - m_Path: Carronade_Rig/BreachRope9.L/BreachRope9.L_end
    m_Weight: 1
  - m_Path: Carronade_Rig/BreachRope9.R
    m_Weight: 1
  - m_Path: Carronade_Rig/BreachRope9.R/BreachRope9.R_end
    m_Weight: 1
  - m_Path: Carronade_LOD0
    m_Weight: 0
  - m_Path: Carronade_LOD1
    m_Weight: 0
  - m_Path: Carronade_LOD2
    m_Weight: 0
  - m_Path: Carronade_LOD3
    m_Weight: 0
  - m_Path: Carronade_LOD4
    m_Weight: 0
