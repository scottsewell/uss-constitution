%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: Cannon_AnimMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Cannon_Rig
    m_Weight: 1
  - m_Path: Cannon_Rig/root
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelBack.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelBack.L/WheelBack.L_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelFront.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelFront.L/WheelFront.L_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/MainRope1.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/MainRope1.L/MainRope1.L_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelBack.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelBack.R/WheelBack.R_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelFront.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/WheelFront.R/WheelFront.R_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/MainRope1.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/MainRope1.R/MainRope1.R_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope1.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope1.R/BreachRope2.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope1.R/BreachRope2.R/BreachRope2.R_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope3.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope3.R/BreachRope4.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope3.R/BreachRope4.R/BreachRope4.R_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R/BreachRope11.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R/BreachRope11.R/BreachRope12.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R/BreachRope11.R/BreachRope12.R/BreachRope13.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R/BreachRope11.R/BreachRope12.R/BreachRope13.R/BreachRope14.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R/BreachRope11.R/BreachRope12.R/BreachRope13.R/BreachRope14.R/BreachRope15.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R/BreachRope11.R/BreachRope12.R/BreachRope13.R/BreachRope14.R/BreachRope15.R/BreachRope16.R
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.R/BreachRope6.R/BreachRope7.R/BreachRope8.R/BreachRope9.R/BreachRope10.R/BreachRope11.R/BreachRope12.R/BreachRope13.R/BreachRope14.R/BreachRope15.R/BreachRope16.R/BreachRope16.R_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope1.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope1.L/BreachRope2.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope1.L/BreachRope2.L/BreachRope2.L_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope3.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope3.L/BreachRope4.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope3.L/BreachRope4.L/BreachRope4.L_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L/BreachRope11.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L/BreachRope11.L/BreachRope12.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L/BreachRope11.L/BreachRope12.L/BreachRope13.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L/BreachRope11.L/BreachRope12.L/BreachRope13.L/BreachRope14.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L/BreachRope11.L/BreachRope12.L/BreachRope13.L/BreachRope14.L/BreachRope15.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L/BreachRope11.L/BreachRope12.L/BreachRope13.L/BreachRope14.L/BreachRope15.L/BreachRope16.L
    m_Weight: 1
  - m_Path: Cannon_Rig/root/main/BreachRope5.L/BreachRope6.L/BreachRope7.L/BreachRope8.L/BreachRope9.L/BreachRope10.L/BreachRope11.L/BreachRope12.L/BreachRope13.L/BreachRope14.L/BreachRope15.L/BreachRope16.L/BreachRope16.L_end
    m_Weight: 1
  - m_Path: Cannon_Rig/root/MainRopeAchor.L
    m_Weight: 0
  - m_Path: Cannon_Rig/root/MainRopeAchor.L/MainRope2.L
    m_Weight: 0
  - m_Path: Cannon_Rig/root/MainRopeAchor.L/MainRope2.L/MainRope2.L_end
    m_Weight: 0
  - m_Path: Cannon_Rig/root/MainRopeAchor.R
    m_Weight: 0
  - m_Path: Cannon_Rig/root/MainRopeAchor.R/MainRope2.R
    m_Weight: 0
  - m_Path: Cannon_Rig/root/MainRopeAchor.R/MainRope2.R/MainRope2.R_end
    m_Weight: 0
  - m_Path: Cannon_LOD0
    m_Weight: 0
  - m_Path: Cannon_LOD1
    m_Weight: 0
  - m_Path: Cannon_LOD3
    m_Weight: 0
  - m_Path: Cannon_LOD2
    m_Weight: 0
  - m_Path: Cannon_LOD4
    m_Weight: 0
